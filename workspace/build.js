const { exec } = require('child_process');
const fse = require('fs-extra');

// Method to copy assets folder into 'dist' folder after build
const copyAssetsFolder = () => {
	var srcPath = __dirname + "\\projects\\ct-template\\src\\assets";
	var distPath = __dirname + "\\dist\\ct-template\\assets";

	console.log('Copying assets folder...');
	console.log(" - from:", srcPath);
	console.log(" - to:  ", distPath);

	fse.copy(srcPath, distPath, function (err) {
		if (err) {
			console.error(err);
		} else {
			console.log("Folder successfully copied.");
		}
	});
}

// Method to run the build
const runBuild = () => {
	exec("ng build ct-template", (error, stdout, stderr) => {
		if (error) {
			console.log(`error: ${error.message}`);
			return;
		}
		if (stderr) {
			console.log(`stderr: ${stderr}`);
			return;
		}
		console.log(`${stdout}`);
	
		copyAssetsFolder();
	});
}

const init = () => {
	runBuild();
}

init();
