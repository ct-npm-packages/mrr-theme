import { Injectable } from '@angular/core';

import { SelectItem } from '../interfaces/ct-template.interface';

@Injectable()
export class CTTemplateService {

	constructor() { }

	setAutoFocus(elementRef: any, isViewChild = true, elementType = '') {
		if (elementRef) {
			setTimeout(() => {
				if (elementType) {
					elementType = elementType.toLowerCase();
					if (elementType === 'multiselect') {
						elementRef.containerViewChild.nativeElement.click();
						elementRef.hide();
					}
				} else if (isViewChild) {
					elementRef.focus();
				} else {
					const element = document.querySelector(elementRef);
					if (element) {
						element.focus();
					}
				}
			}, 0);
		}
	}

	setSelectedOptionsLabel(selectedValues: string[], options: any[]) {
		const values = [];
		let label: string = '';

		if (selectedValues.length) {
			selectedValues.forEach((value: string) => {
				values.push(
					options
						.filter((item: SelectItem) => item.value === value)
						.map((item: SelectItem) => item.label)
				);
			});

			label = values[0] + (values.length > 1 ? '...' : '');
			setTimeout(() => {
				this.adjustMultiSelectDropdownLabel('with-selection-multiselect-1', values.length);
			}, 0);
		}
		return label;
	}

	setSelectedOptionsLabelTitle(selectedValues: string[], options: any[]) {
		const values = [];
		let title: string = '';

		if (selectedValues.length) {
			selectedValues.forEach((value: string) => {
				values.push(
					options
						.filter((item: SelectItem) => item.value === value)
						.map((item: SelectItem) => item.label)
				);
			});

			title = values.join('\n');
		}
		return title;
	}

	adjustMultiSelectDropdownLabel(elementId = '', optionsLength = 0) {
		if (elementId) {
			setTimeout(() => {
				const labelElementWrapper = document.querySelector('#' + elementId + ' .ui-multiselect') as any;
				const labelElement = document.querySelector('#' + elementId + ' .ui-multiselected-item-token span') as any;
				const minusWidth = optionsLength > 1 ? 37 : 30;
				if (labelElementWrapper && labelElement) {
					if (labelElement.offsetWidth > (labelElementWrapper.offsetWidth - minusWidth)) {
						labelElement.style['max-width'] = (labelElementWrapper.offsetWidth - minusWidth) + 'px';
					}
				}
			}, 0);
		}
	}
}
