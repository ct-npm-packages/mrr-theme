import { Directive, ElementRef, Renderer2, HostListener, Input } from '@angular/core';

// Directive to show tooltip text with contents in a list with elements in vertical order.
// Currently caters to only p-multiselect element.
@Directive({
	selector: '[ctTemplateTooltip]'
})
export class TooltipDirective {
	@Input() ngModel: any;

	constructor(private el: ElementRef,
		private renderer: Renderer2) {
	}

	@HostListener('mouseover') onMouseOver() {
		const multiSelectContainer: HTMLElement = this.el.nativeElement.querySelector('.ui-multiselect-label-container') as HTMLElement;
		const memberNameContainer: HTMLElement = this.el.nativeElement.querySelector('.memberName') as HTMLElement;
		const providerNameContainer: HTMLElement = this.el.nativeElement.querySelector('.providerName') as HTMLElement;

		if (multiSelectContainer) {
			let title = multiSelectContainer.getAttribute('title');
			const hasFilterList = this.el.nativeElement.classList.contains('filteredTooltipList');
			if (hasFilterList && this.ngModel) {
				title = '';
				this.ngModel.forEach((item: any) => {
					title += item.value + '\n';
				});
			} else {
				title = title.split(', ').join('\n');
			}
			this.renderer.setProperty(multiSelectContainer, 'title', title);
		} else if (memberNameContainer) {
			const title = memberNameContainer.getAttribute('title');
			const titleArr = title.split('- ');

			if (titleArr.length > 1) {
				this.renderer.setProperty(memberNameContainer, 'title', titleArr.join('\n'));
			}
		} else if (providerNameContainer) {
			const title = providerNameContainer.getAttribute('title');
			const titleArr = title.split('- ');

			if (titleArr.length > 1) {
				const providerNpi = titleArr.pop();
				this.renderer.setProperty(providerNameContainer, 'title', titleArr.pop() + '\n' + providerNpi);
			}
		} else {
			const title = this.el.nativeElement.getAttribute('title');
			const titleArr = title.split(', ');

			if (titleArr.length > 1) {
				const titleString = titleArr.join('\n');
				this.renderer.setProperty(this.el.nativeElement, 'title', titleString);
			}
		}
	}
}
