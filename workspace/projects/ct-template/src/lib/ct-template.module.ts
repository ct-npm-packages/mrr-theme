// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Components
import { DialogBoxCloseIconComponent } from './components/dialog-box/close-icon/dialog-box.close-icon.component';
import { FilterCountComponent } from './components/filter-count/filter-count.component';

// Directives
import { TooltipDirective } from './directives/tooltip.directive';

// Services
import { CTTemplateService } from './services/ct-template.service';

@NgModule({
	declarations: [
		DialogBoxCloseIconComponent,
		FilterCountComponent,
		TooltipDirective
	],
	imports: [
		CommonModule
	],
	exports: [
		DialogBoxCloseIconComponent,
		FilterCountComponent,
		TooltipDirective
	],
	providers: [
		CTTemplateService
	]
})
export class CTTemplateModule { }
