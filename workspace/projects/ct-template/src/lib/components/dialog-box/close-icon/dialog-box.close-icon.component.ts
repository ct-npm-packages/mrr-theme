import { Component, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'ct-template-dialog-box-close-icon',
	templateUrl: './dialog-box.close-icon.component.html',
})
export class DialogBoxCloseIconComponent {

	@Output() onClose: EventEmitter<MouseEvent> = new EventEmitter();

	constructor() { }

	close($event: MouseEvent) {
		this.onClose.emit($event);
	}
}
