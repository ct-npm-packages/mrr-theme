const { exec } = require('child_process');
const fse = require('fs-extra');
const path = require('path');

// Method to remove the build contents from main library folder path
const removeOldBuildContents = () => {
	var srcPath = __dirname + "\\..\\";
	const excludedlist = ['.git', 'workspace'];

	console.log("Deleting old build contents from library folder...");
	console.log("Path:", srcPath);

	fse.readdir(srcPath, (err, files) => {
		if (err) {
			console.log(err);
		} else {
			files.forEach(file => {
				const filePath = path.join(srcPath, file);
				if (excludedlist.indexOf(file) === -1) {
					if (fse.lstatSync(filePath).isDirectory()) {
						fse.rmdirSync(filePath, { recursive: true }, (err) => {});
					} else {
						fse.unlinkSync(filePath);
					}
				}
			});

			console.log("Deleted successfully.\n");

			publishNewBuildContents();
		}
	});
}

// Method to copy build ('dist') folder contents to main library folder path
const publishNewBuildContents = () => {
	var srcPath = __dirname + "\\dist\\ct-template";
	var distPath = __dirname + "\\..\\";
	
	console.log('Publishing new build contents to library folder...');
	console.log(" - from:", srcPath);
	console.log(" - to:  ", distPath);

	fse.copy(srcPath, distPath, function (err) {
		if (err) {
			console.error(err);
		} else {
			console.log("Published successfully.");
		}
	});
}

const init = () => {
	removeOldBuildContents();
}

init();
