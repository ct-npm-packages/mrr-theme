import { EventEmitter } from '@angular/core';
export declare class DialogBoxCloseIconComponent {
    onClose: EventEmitter<MouseEvent>;
    constructor();
    close($event: MouseEvent): void;
}
