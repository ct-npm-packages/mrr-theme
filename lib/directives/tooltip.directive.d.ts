import { ElementRef, Renderer2 } from '@angular/core';
export declare class TooltipDirective {
    private el;
    private renderer;
    ngModel: any;
    constructor(el: ElementRef, renderer: Renderer2);
    onMouseOver(): void;
}
