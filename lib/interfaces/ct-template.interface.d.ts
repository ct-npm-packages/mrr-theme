export interface MultiSelectToggleItem {
    itemValue: string;
    originalEvent: MouseEvent;
    value: string[];
}
export interface SelectItem {
    label?: string;
    value: any;
    styleClass?: string;
    icon?: string;
    title?: string;
    disabled?: boolean;
}
