export declare class CTTemplateService {
    constructor();
    setAutoFocus(elementRef: any, isViewChild?: boolean, elementType?: string): void;
    setSelectedOptionsLabel(selectedValues: string[], options: any[]): string;
    setSelectedOptionsLabelTitle(selectedValues: string[], options: any[]): string;
    adjustMultiSelectDropdownLabel(elementId?: string, optionsLength?: number): void;
}
