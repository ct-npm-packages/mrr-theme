(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/common'), require('@angular/core')) :
    typeof define === 'function' && define.amd ? define('ct-template', ['exports', '@angular/common', '@angular/core'], factory) :
    (factory((global['ct-template'] = {}),global.ng.common,global.ng.core));
}(this, (function (exports,common,core) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/components/dialog-box/close-icon/dialog-box.close-icon.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var DialogBoxCloseIconComponent = /** @class */ (function () {
        function DialogBoxCloseIconComponent() {
            this.onClose = new core.EventEmitter();
        }
        /**
         * @param {?} $event
         * @return {?}
         */
        DialogBoxCloseIconComponent.prototype.close = /**
         * @param {?} $event
         * @return {?}
         */
            function ($event) {
                this.onClose.emit($event);
            };
        DialogBoxCloseIconComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'ct-template-dialog-box-close-icon',
                        template: "<button (click)=\"close($event)\" class=\"ui-dialog-titlebar-icon ui-dialog-titlebar-close ui-corner-all\">\r\n\t<span class=\"pi pi-times\"></span>\r\n</button>"
                    }] }
        ];
        /** @nocollapse */
        DialogBoxCloseIconComponent.ctorParameters = function () { return []; };
        DialogBoxCloseIconComponent.propDecorators = {
            onClose: [{ type: core.Output }]
        };
        return DialogBoxCloseIconComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/components/filter-count/filter-count.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var FilterCountComponent = /** @class */ (function () {
        function FilterCountComponent() {
            this.selectedFilterCount = 0;
            this.filterTitle = '';
        }
        /**
         * @return {?}
         */
        FilterCountComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        FilterCountComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'ct-template-filter-count',
                        template: "<span *ngIf=\"selectedFilterCount && selectedFilterCount > 0\" class=\"badge badge-info badge-display\"\r\n\t[ngClass]=\"{'badge-display-twodigit':selectedFilterCount && selectedFilterCount > 9}\"\r\n\ttitle=\"{{filterTitle}}\">{{selectedFilterCount}}\r\n</span>",
                        styles: [".badge-display{height:16px;width:17px;display:inline-block!important;padding-top:3px!important}.badge-display-twodigit{width:19px}"]
                    }] }
        ];
        /** @nocollapse */
        FilterCountComponent.ctorParameters = function () { return []; };
        FilterCountComponent.propDecorators = {
            selectedFilterCount: [{ type: core.Input }],
            filterTitle: [{ type: core.Input }]
        };
        return FilterCountComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/directives/tooltip.directive.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    // Directive to show tooltip text with contents in a list with elements in vertical order.
    // Currently caters to only p-multiselect element.
    var TooltipDirective = /** @class */ (function () {
        function TooltipDirective(el, renderer) {
            this.el = el;
            this.renderer = renderer;
        }
        /**
         * @return {?}
         */
        TooltipDirective.prototype.onMouseOver = /**
         * @return {?}
         */
            function () {
                /** @type {?} */
                var multiSelectContainer = ( /** @type {?} */(this.el.nativeElement.querySelector('.ui-multiselect-label-container')));
                /** @type {?} */
                var memberNameContainer = ( /** @type {?} */(this.el.nativeElement.querySelector('.memberName')));
                /** @type {?} */
                var providerNameContainer = ( /** @type {?} */(this.el.nativeElement.querySelector('.providerName')));
                if (multiSelectContainer) {
                    /** @type {?} */
                    var title_1 = multiSelectContainer.getAttribute('title');
                    /** @type {?} */
                    var hasFilterList = this.el.nativeElement.classList.contains('filteredTooltipList');
                    if (hasFilterList && this.ngModel) {
                        title_1 = '';
                        this.ngModel.forEach(( /**
                         * @param {?} item
                         * @return {?}
                         */function (item) {
                            title_1 += item.value + '\n';
                        }));
                    }
                    else {
                        title_1 = title_1.split(', ').join('\n');
                    }
                    this.renderer.setProperty(multiSelectContainer, 'title', title_1);
                }
                else if (memberNameContainer) {
                    /** @type {?} */
                    var title = memberNameContainer.getAttribute('title');
                    /** @type {?} */
                    var titleArr = title.split('- ');
                    if (titleArr.length > 1) {
                        this.renderer.setProperty(memberNameContainer, 'title', titleArr.join('\n'));
                    }
                }
                else if (providerNameContainer) {
                    /** @type {?} */
                    var title = providerNameContainer.getAttribute('title');
                    /** @type {?} */
                    var titleArr = title.split('- ');
                    if (titleArr.length > 1) {
                        /** @type {?} */
                        var providerNpi = titleArr.pop();
                        this.renderer.setProperty(providerNameContainer, 'title', titleArr.pop() + '\n' + providerNpi);
                    }
                }
                else {
                    /** @type {?} */
                    var title = this.el.nativeElement.getAttribute('title');
                    /** @type {?} */
                    var titleArr = title.split(', ');
                    if (titleArr.length > 1) {
                        /** @type {?} */
                        var titleString = titleArr.join('\n');
                        this.renderer.setProperty(this.el.nativeElement, 'title', titleString);
                    }
                }
            };
        TooltipDirective.decorators = [
            { type: core.Directive, args: [{
                        selector: '[ctTemplateTooltip]'
                    },] }
        ];
        /** @nocollapse */
        TooltipDirective.ctorParameters = function () {
            return [
                { type: core.ElementRef },
                { type: core.Renderer2 }
            ];
        };
        TooltipDirective.propDecorators = {
            ngModel: [{ type: core.Input }],
            onMouseOver: [{ type: core.HostListener, args: ['mouseover',] }]
        };
        return TooltipDirective;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/services/ct-template.service.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var CTTemplateService = /** @class */ (function () {
        function CTTemplateService() {
        }
        /**
         * @param {?} elementRef
         * @param {?=} isViewChild
         * @param {?=} elementType
         * @return {?}
         */
        CTTemplateService.prototype.setAutoFocus = /**
         * @param {?} elementRef
         * @param {?=} isViewChild
         * @param {?=} elementType
         * @return {?}
         */
            function (elementRef, isViewChild, elementType) {
                if (isViewChild === void 0) {
                    isViewChild = true;
                }
                if (elementType === void 0) {
                    elementType = '';
                }
                if (elementRef) {
                    setTimeout(( /**
                     * @return {?}
                     */function () {
                        if (elementType) {
                            elementType = elementType.toLowerCase();
                            if (elementType === 'multiselect') {
                                elementRef.containerViewChild.nativeElement.click();
                                elementRef.hide();
                            }
                        }
                        else if (isViewChild) {
                            elementRef.focus();
                        }
                        else {
                            /** @type {?} */
                            var element = document.querySelector(elementRef);
                            if (element) {
                                element.focus();
                            }
                        }
                    }), 0);
                }
            };
        /**
         * @param {?} selectedValues
         * @param {?} options
         * @return {?}
         */
        CTTemplateService.prototype.setSelectedOptionsLabel = /**
         * @param {?} selectedValues
         * @param {?} options
         * @return {?}
         */
            function (selectedValues, options) {
                var _this = this;
                /** @type {?} */
                var values = [];
                /** @type {?} */
                var label = '';
                if (selectedValues.length) {
                    selectedValues.forEach(( /**
                     * @param {?} value
                     * @return {?}
                     */function (value) {
                        values.push(options
                            .filter(( /**
                     * @param {?} item
                     * @return {?}
                     */function (item) { return item.value === value; }))
                            .map(( /**
                     * @param {?} item
                     * @return {?}
                     */function (item) { return item.label; })));
                    }));
                    label = values[0] + (values.length > 1 ? '...' : '');
                    setTimeout(( /**
                     * @return {?}
                     */function () {
                        _this.adjustMultiSelectDropdownLabel('with-selection-multiselect-1', values.length);
                    }), 0);
                }
                return label;
            };
        /**
         * @param {?} selectedValues
         * @param {?} options
         * @return {?}
         */
        CTTemplateService.prototype.setSelectedOptionsLabelTitle = /**
         * @param {?} selectedValues
         * @param {?} options
         * @return {?}
         */
            function (selectedValues, options) {
                /** @type {?} */
                var values = [];
                /** @type {?} */
                var title = '';
                if (selectedValues.length) {
                    selectedValues.forEach(( /**
                     * @param {?} value
                     * @return {?}
                     */function (value) {
                        values.push(options
                            .filter(( /**
                     * @param {?} item
                     * @return {?}
                     */function (item) { return item.value === value; }))
                            .map(( /**
                     * @param {?} item
                     * @return {?}
                     */function (item) { return item.label; })));
                    }));
                    title = values.join('\n');
                }
                return title;
            };
        /**
         * @param {?=} elementId
         * @param {?=} optionsLength
         * @return {?}
         */
        CTTemplateService.prototype.adjustMultiSelectDropdownLabel = /**
         * @param {?=} elementId
         * @param {?=} optionsLength
         * @return {?}
         */
            function (elementId, optionsLength) {
                if (elementId === void 0) {
                    elementId = '';
                }
                if (optionsLength === void 0) {
                    optionsLength = 0;
                }
                if (elementId) {
                    setTimeout(( /**
                     * @return {?}
                     */function () {
                        /** @type {?} */
                        var labelElementWrapper = ( /** @type {?} */(document.querySelector('#' + elementId + ' .ui-multiselect')));
                        /** @type {?} */
                        var labelElement = ( /** @type {?} */(document.querySelector('#' + elementId + ' .ui-multiselected-item-token span')));
                        /** @type {?} */
                        var minusWidth = optionsLength > 1 ? 37 : 30;
                        if (labelElementWrapper && labelElement) {
                            if (labelElement.offsetWidth > (labelElementWrapper.offsetWidth - minusWidth)) {
                                labelElement.style['max-width'] = (labelElementWrapper.offsetWidth - minusWidth) + 'px';
                            }
                        }
                    }), 0);
                }
            };
        CTTemplateService.decorators = [
            { type: core.Injectable }
        ];
        /** @nocollapse */
        CTTemplateService.ctorParameters = function () { return []; };
        return CTTemplateService;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/ct-template.module.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var CTTemplateModule = /** @class */ (function () {
        function CTTemplateModule() {
        }
        CTTemplateModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [
                            DialogBoxCloseIconComponent,
                            FilterCountComponent,
                            TooltipDirective
                        ],
                        imports: [
                            common.CommonModule
                        ],
                        exports: [
                            DialogBoxCloseIconComponent,
                            FilterCountComponent,
                            TooltipDirective
                        ],
                        providers: [
                            CTTemplateService
                        ]
                    },] }
        ];
        return CTTemplateModule;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: public-api.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * Generated from: ct-template.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    exports.CTTemplateModule = CTTemplateModule;
    exports.CTTemplateService = CTTemplateService;
    exports.ɵa = DialogBoxCloseIconComponent;
    exports.ɵb = FilterCountComponent;
    exports.ɵc = TooltipDirective;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=ct-template.umd.js.map