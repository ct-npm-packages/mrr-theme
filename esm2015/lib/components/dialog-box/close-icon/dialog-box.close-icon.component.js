/**
 * @fileoverview added by tsickle
 * Generated from: lib/components/dialog-box/close-icon/dialog-box.close-icon.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Output, EventEmitter } from '@angular/core';
export class DialogBoxCloseIconComponent {
    constructor() {
        this.onClose = new EventEmitter();
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    close($event) {
        this.onClose.emit($event);
    }
}
DialogBoxCloseIconComponent.decorators = [
    { type: Component, args: [{
                selector: 'ct-template-dialog-box-close-icon',
                template: "<button (click)=\"close($event)\" class=\"ui-dialog-titlebar-icon ui-dialog-titlebar-close ui-corner-all\">\r\n\t<span class=\"pi pi-times\"></span>\r\n</button>"
            }] }
];
/** @nocollapse */
DialogBoxCloseIconComponent.ctorParameters = () => [];
DialogBoxCloseIconComponent.propDecorators = {
    onClose: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    DialogBoxCloseIconComponent.prototype.onClose;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlhbG9nLWJveC5jbG9zZS1pY29uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2N0LXRlbXBsYXRlLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvZGlhbG9nLWJveC9jbG9zZS1pY29uL2RpYWxvZy1ib3guY2xvc2UtaWNvbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFNaEUsTUFBTSxPQUFPLDJCQUEyQjtJQUl2QztRQUZVLFlBQU8sR0FBNkIsSUFBSSxZQUFZLEVBQUUsQ0FBQztJQUVqRCxDQUFDOzs7OztJQUVqQixLQUFLLENBQUMsTUFBa0I7UUFDdkIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDM0IsQ0FBQzs7O1lBWkQsU0FBUyxTQUFDO2dCQUNWLFFBQVEsRUFBRSxtQ0FBbUM7Z0JBQzdDLDZLQUFxRDthQUNyRDs7Ozs7c0JBR0MsTUFBTTs7OztJQUFQLDhDQUFpRSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG5cdHNlbGVjdG9yOiAnY3QtdGVtcGxhdGUtZGlhbG9nLWJveC1jbG9zZS1pY29uJyxcclxuXHR0ZW1wbGF0ZVVybDogJy4vZGlhbG9nLWJveC5jbG9zZS1pY29uLmNvbXBvbmVudC5odG1sJyxcclxufSlcclxuZXhwb3J0IGNsYXNzIERpYWxvZ0JveENsb3NlSWNvbkNvbXBvbmVudCB7XHJcblxyXG5cdEBPdXRwdXQoKSBvbkNsb3NlOiBFdmVudEVtaXR0ZXI8TW91c2VFdmVudD4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcblxyXG5cdGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG5cdGNsb3NlKCRldmVudDogTW91c2VFdmVudCkge1xyXG5cdFx0dGhpcy5vbkNsb3NlLmVtaXQoJGV2ZW50KTtcclxuXHR9XHJcbn1cclxuIl19