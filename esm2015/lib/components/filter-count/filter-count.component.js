/**
 * @fileoverview added by tsickle
 * Generated from: lib/components/filter-count/filter-count.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
export class FilterCountComponent {
    constructor() {
        this.selectedFilterCount = 0;
        this.filterTitle = '';
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
FilterCountComponent.decorators = [
    { type: Component, args: [{
                selector: 'ct-template-filter-count',
                template: "<span *ngIf=\"selectedFilterCount && selectedFilterCount > 0\" class=\"badge badge-info badge-display\"\r\n\t[ngClass]=\"{'badge-display-twodigit':selectedFilterCount && selectedFilterCount > 9}\"\r\n\ttitle=\"{{filterTitle}}\">{{selectedFilterCount}}\r\n</span>",
                styles: [".badge-display{height:16px;width:17px;display:inline-block!important;padding-top:3px!important}.badge-display-twodigit{width:19px}"]
            }] }
];
/** @nocollapse */
FilterCountComponent.ctorParameters = () => [];
FilterCountComponent.propDecorators = {
    selectedFilterCount: [{ type: Input }],
    filterTitle: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    FilterCountComponent.prototype.selectedFilterCount;
    /** @type {?} */
    FilterCountComponent.prototype.filterTitle;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsdGVyLWNvdW50LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2N0LXRlbXBsYXRlLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvZmlsdGVyLWNvdW50L2ZpbHRlci1jb3VudC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQU96RCxNQUFNLE9BQU8sb0JBQW9CO0lBS2hDO1FBSFMsd0JBQW1CLEdBQVcsQ0FBQyxDQUFDO1FBQ2hDLGdCQUFXLEdBQVcsRUFBRSxDQUFDO0lBRWxCLENBQUM7Ozs7SUFFakIsUUFBUTtJQUNSLENBQUM7OztZQWJELFNBQVMsU0FBQztnQkFDVixRQUFRLEVBQUUsMEJBQTBCO2dCQUNwQyxrUkFBNEM7O2FBRTVDOzs7OztrQ0FHQyxLQUFLOzBCQUNMLEtBQUs7Ozs7SUFETixtREFBeUM7O0lBQ3pDLDJDQUFrQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcblx0c2VsZWN0b3I6ICdjdC10ZW1wbGF0ZS1maWx0ZXItY291bnQnLFxyXG5cdHRlbXBsYXRlVXJsOiAnLi9maWx0ZXItY291bnQuY29tcG9uZW50Lmh0bWwnLFxyXG5cdHN0eWxlVXJsczogWycuL2ZpbHRlci1jb3VudC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGaWx0ZXJDb3VudENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG5cdEBJbnB1dCgpIHNlbGVjdGVkRmlsdGVyQ291bnQ6IE51bWJlciA9IDA7XHJcblx0QElucHV0KCkgZmlsdGVyVGl0bGU6IFN0cmluZyA9ICcnO1xyXG5cclxuXHRjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuXHRuZ09uSW5pdCgpIHtcclxuXHR9XHJcblxyXG59XHJcbiJdfQ==