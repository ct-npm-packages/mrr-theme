/**
 * @fileoverview added by tsickle
 * Generated from: lib/directives/tooltip.directive.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, ElementRef, Renderer2, HostListener, Input } from '@angular/core';
// Directive to show tooltip text with contents in a list with elements in vertical order.
// Currently caters to only p-multiselect element.
export class TooltipDirective {
    /**
     * @param {?} el
     * @param {?} renderer
     */
    constructor(el, renderer) {
        this.el = el;
        this.renderer = renderer;
    }
    /**
     * @return {?}
     */
    onMouseOver() {
        /** @type {?} */
        const multiSelectContainer = (/** @type {?} */ (this.el.nativeElement.querySelector('.ui-multiselect-label-container')));
        /** @type {?} */
        const memberNameContainer = (/** @type {?} */ (this.el.nativeElement.querySelector('.memberName')));
        /** @type {?} */
        const providerNameContainer = (/** @type {?} */ (this.el.nativeElement.querySelector('.providerName')));
        if (multiSelectContainer) {
            /** @type {?} */
            let title = multiSelectContainer.getAttribute('title');
            /** @type {?} */
            const hasFilterList = this.el.nativeElement.classList.contains('filteredTooltipList');
            if (hasFilterList && this.ngModel) {
                title = '';
                this.ngModel.forEach((/**
                 * @param {?} item
                 * @return {?}
                 */
                (item) => {
                    title += item.value + '\n';
                }));
            }
            else {
                title = title.split(', ').join('\n');
            }
            this.renderer.setProperty(multiSelectContainer, 'title', title);
        }
        else if (memberNameContainer) {
            /** @type {?} */
            const title = memberNameContainer.getAttribute('title');
            /** @type {?} */
            const titleArr = title.split('- ');
            if (titleArr.length > 1) {
                this.renderer.setProperty(memberNameContainer, 'title', titleArr.join('\n'));
            }
        }
        else if (providerNameContainer) {
            /** @type {?} */
            const title = providerNameContainer.getAttribute('title');
            /** @type {?} */
            const titleArr = title.split('- ');
            if (titleArr.length > 1) {
                /** @type {?} */
                const providerNpi = titleArr.pop();
                this.renderer.setProperty(providerNameContainer, 'title', titleArr.pop() + '\n' + providerNpi);
            }
        }
        else {
            /** @type {?} */
            const title = this.el.nativeElement.getAttribute('title');
            /** @type {?} */
            const titleArr = title.split(', ');
            if (titleArr.length > 1) {
                /** @type {?} */
                const titleString = titleArr.join('\n');
                this.renderer.setProperty(this.el.nativeElement, 'title', titleString);
            }
        }
    }
}
TooltipDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ctTemplateTooltip]'
            },] }
];
/** @nocollapse */
TooltipDirective.ctorParameters = () => [
    { type: ElementRef },
    { type: Renderer2 }
];
TooltipDirective.propDecorators = {
    ngModel: [{ type: Input }],
    onMouseOver: [{ type: HostListener, args: ['mouseover',] }]
};
if (false) {
    /** @type {?} */
    TooltipDirective.prototype.ngModel;
    /**
     * @type {?}
     * @private
     */
    TooltipDirective.prototype.el;
    /**
     * @type {?}
     * @private
     */
    TooltipDirective.prototype.renderer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9vbHRpcC5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9jdC10ZW1wbGF0ZS8iLCJzb3VyY2VzIjpbImxpYi9kaXJlY3RpdmVzL3Rvb2x0aXAuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7OztBQU90RixNQUFNLE9BQU8sZ0JBQWdCOzs7OztJQUc1QixZQUFvQixFQUFjLEVBQ3pCLFFBQW1CO1FBRFIsT0FBRSxHQUFGLEVBQUUsQ0FBWTtRQUN6QixhQUFRLEdBQVIsUUFBUSxDQUFXO0lBQzVCLENBQUM7Ozs7SUFFMEIsV0FBVzs7Y0FDL0Isb0JBQW9CLEdBQWdCLG1CQUFBLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxpQ0FBaUMsQ0FBQyxFQUFlOztjQUN6SCxtQkFBbUIsR0FBZ0IsbUJBQUEsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxFQUFlOztjQUNwRyxxQkFBcUIsR0FBZ0IsbUJBQUEsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBQyxFQUFlO1FBRTlHLElBQUksb0JBQW9CLEVBQUU7O2dCQUNyQixLQUFLLEdBQUcsb0JBQW9CLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQzs7a0JBQ2hELGFBQWEsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLHFCQUFxQixDQUFDO1lBQ3JGLElBQUksYUFBYSxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7Z0JBQ2xDLEtBQUssR0FBRyxFQUFFLENBQUM7Z0JBQ1gsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPOzs7O2dCQUFDLENBQUMsSUFBUyxFQUFFLEVBQUU7b0JBQ2xDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztnQkFDNUIsQ0FBQyxFQUFDLENBQUM7YUFDSDtpQkFBTTtnQkFDTixLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDckM7WUFDRCxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxvQkFBb0IsRUFBRSxPQUFPLEVBQUUsS0FBSyxDQUFDLENBQUM7U0FDaEU7YUFBTSxJQUFJLG1CQUFtQixFQUFFOztrQkFDekIsS0FBSyxHQUFHLG1CQUFtQixDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUM7O2tCQUNqRCxRQUFRLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUM7WUFFbEMsSUFBSSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDeEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsbUJBQW1CLEVBQUUsT0FBTyxFQUFFLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQzthQUM3RTtTQUNEO2FBQU0sSUFBSSxxQkFBcUIsRUFBRTs7a0JBQzNCLEtBQUssR0FBRyxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDOztrQkFDbkQsUUFBUSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDO1lBRWxDLElBQUksUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7O3NCQUNsQixXQUFXLEdBQUcsUUFBUSxDQUFDLEdBQUcsRUFBRTtnQkFDbEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMscUJBQXFCLEVBQUUsT0FBTyxFQUFFLFFBQVEsQ0FBQyxHQUFHLEVBQUUsR0FBRyxJQUFJLEdBQUcsV0FBVyxDQUFDLENBQUM7YUFDL0Y7U0FDRDthQUFNOztrQkFDQSxLQUFLLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQzs7a0JBQ25ELFFBQVEsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztZQUVsQyxJQUFJLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFOztzQkFDbEIsV0FBVyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO2dCQUN2QyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsRUFBRSxPQUFPLEVBQUUsV0FBVyxDQUFDLENBQUM7YUFDdkU7U0FDRDtJQUNGLENBQUM7OztZQW5ERCxTQUFTLFNBQUM7Z0JBQ1YsUUFBUSxFQUFFLHFCQUFxQjthQUMvQjs7OztZQU5tQixVQUFVO1lBQUUsU0FBUzs7O3NCQVF2QyxLQUFLOzBCQU1MLFlBQVksU0FBQyxXQUFXOzs7O0lBTnpCLG1DQUFzQjs7Ozs7SUFFViw4QkFBc0I7Ozs7O0lBQ2pDLG9DQUEyQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpcmVjdGl2ZSwgRWxlbWVudFJlZiwgUmVuZGVyZXIyLCBIb3N0TGlzdGVuZXIsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG4vLyBEaXJlY3RpdmUgdG8gc2hvdyB0b29sdGlwIHRleHQgd2l0aCBjb250ZW50cyBpbiBhIGxpc3Qgd2l0aCBlbGVtZW50cyBpbiB2ZXJ0aWNhbCBvcmRlci5cclxuLy8gQ3VycmVudGx5IGNhdGVycyB0byBvbmx5IHAtbXVsdGlzZWxlY3QgZWxlbWVudC5cclxuQERpcmVjdGl2ZSh7XHJcblx0c2VsZWN0b3I6ICdbY3RUZW1wbGF0ZVRvb2x0aXBdJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgVG9vbHRpcERpcmVjdGl2ZSB7XHJcblx0QElucHV0KCkgbmdNb2RlbDogYW55O1xyXG5cclxuXHRjb25zdHJ1Y3Rvcihwcml2YXRlIGVsOiBFbGVtZW50UmVmLFxyXG5cdFx0cHJpdmF0ZSByZW5kZXJlcjogUmVuZGVyZXIyKSB7XHJcblx0fVxyXG5cclxuXHRASG9zdExpc3RlbmVyKCdtb3VzZW92ZXInKSBvbk1vdXNlT3ZlcigpIHtcclxuXHRcdGNvbnN0IG11bHRpU2VsZWN0Q29udGFpbmVyOiBIVE1MRWxlbWVudCA9IHRoaXMuZWwubmF0aXZlRWxlbWVudC5xdWVyeVNlbGVjdG9yKCcudWktbXVsdGlzZWxlY3QtbGFiZWwtY29udGFpbmVyJykgYXMgSFRNTEVsZW1lbnQ7XHJcblx0XHRjb25zdCBtZW1iZXJOYW1lQ29udGFpbmVyOiBIVE1MRWxlbWVudCA9IHRoaXMuZWwubmF0aXZlRWxlbWVudC5xdWVyeVNlbGVjdG9yKCcubWVtYmVyTmFtZScpIGFzIEhUTUxFbGVtZW50O1xyXG5cdFx0Y29uc3QgcHJvdmlkZXJOYW1lQ29udGFpbmVyOiBIVE1MRWxlbWVudCA9IHRoaXMuZWwubmF0aXZlRWxlbWVudC5xdWVyeVNlbGVjdG9yKCcucHJvdmlkZXJOYW1lJykgYXMgSFRNTEVsZW1lbnQ7XHJcblxyXG5cdFx0aWYgKG11bHRpU2VsZWN0Q29udGFpbmVyKSB7XHJcblx0XHRcdGxldCB0aXRsZSA9IG11bHRpU2VsZWN0Q29udGFpbmVyLmdldEF0dHJpYnV0ZSgndGl0bGUnKTtcclxuXHRcdFx0Y29uc3QgaGFzRmlsdGVyTGlzdCA9IHRoaXMuZWwubmF0aXZlRWxlbWVudC5jbGFzc0xpc3QuY29udGFpbnMoJ2ZpbHRlcmVkVG9vbHRpcExpc3QnKTtcclxuXHRcdFx0aWYgKGhhc0ZpbHRlckxpc3QgJiYgdGhpcy5uZ01vZGVsKSB7XHJcblx0XHRcdFx0dGl0bGUgPSAnJztcclxuXHRcdFx0XHR0aGlzLm5nTW9kZWwuZm9yRWFjaCgoaXRlbTogYW55KSA9PiB7XHJcblx0XHRcdFx0XHR0aXRsZSArPSBpdGVtLnZhbHVlICsgJ1xcbic7XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0dGl0bGUgPSB0aXRsZS5zcGxpdCgnLCAnKS5qb2luKCdcXG4nKTtcclxuXHRcdFx0fVxyXG5cdFx0XHR0aGlzLnJlbmRlcmVyLnNldFByb3BlcnR5KG11bHRpU2VsZWN0Q29udGFpbmVyLCAndGl0bGUnLCB0aXRsZSk7XHJcblx0XHR9IGVsc2UgaWYgKG1lbWJlck5hbWVDb250YWluZXIpIHtcclxuXHRcdFx0Y29uc3QgdGl0bGUgPSBtZW1iZXJOYW1lQ29udGFpbmVyLmdldEF0dHJpYnV0ZSgndGl0bGUnKTtcclxuXHRcdFx0Y29uc3QgdGl0bGVBcnIgPSB0aXRsZS5zcGxpdCgnLSAnKTtcclxuXHJcblx0XHRcdGlmICh0aXRsZUFyci5sZW5ndGggPiAxKSB7XHJcblx0XHRcdFx0dGhpcy5yZW5kZXJlci5zZXRQcm9wZXJ0eShtZW1iZXJOYW1lQ29udGFpbmVyLCAndGl0bGUnLCB0aXRsZUFyci5qb2luKCdcXG4nKSk7XHJcblx0XHRcdH1cclxuXHRcdH0gZWxzZSBpZiAocHJvdmlkZXJOYW1lQ29udGFpbmVyKSB7XHJcblx0XHRcdGNvbnN0IHRpdGxlID0gcHJvdmlkZXJOYW1lQ29udGFpbmVyLmdldEF0dHJpYnV0ZSgndGl0bGUnKTtcclxuXHRcdFx0Y29uc3QgdGl0bGVBcnIgPSB0aXRsZS5zcGxpdCgnLSAnKTtcclxuXHJcblx0XHRcdGlmICh0aXRsZUFyci5sZW5ndGggPiAxKSB7XHJcblx0XHRcdFx0Y29uc3QgcHJvdmlkZXJOcGkgPSB0aXRsZUFyci5wb3AoKTtcclxuXHRcdFx0XHR0aGlzLnJlbmRlcmVyLnNldFByb3BlcnR5KHByb3ZpZGVyTmFtZUNvbnRhaW5lciwgJ3RpdGxlJywgdGl0bGVBcnIucG9wKCkgKyAnXFxuJyArIHByb3ZpZGVyTnBpKTtcclxuXHRcdFx0fVxyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0Y29uc3QgdGl0bGUgPSB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQuZ2V0QXR0cmlidXRlKCd0aXRsZScpO1xyXG5cdFx0XHRjb25zdCB0aXRsZUFyciA9IHRpdGxlLnNwbGl0KCcsICcpO1xyXG5cclxuXHRcdFx0aWYgKHRpdGxlQXJyLmxlbmd0aCA+IDEpIHtcclxuXHRcdFx0XHRjb25zdCB0aXRsZVN0cmluZyA9IHRpdGxlQXJyLmpvaW4oJ1xcbicpO1xyXG5cdFx0XHRcdHRoaXMucmVuZGVyZXIuc2V0UHJvcGVydHkodGhpcy5lbC5uYXRpdmVFbGVtZW50LCAndGl0bGUnLCB0aXRsZVN0cmluZyk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcbn1cclxuIl19