/**
 * @fileoverview added by tsickle
 * Generated from: lib/services/ct-template.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
export class CTTemplateService {
    constructor() { }
    /**
     * @param {?} elementRef
     * @param {?=} isViewChild
     * @param {?=} elementType
     * @return {?}
     */
    setAutoFocus(elementRef, isViewChild = true, elementType = '') {
        if (elementRef) {
            setTimeout((/**
             * @return {?}
             */
            () => {
                if (elementType) {
                    elementType = elementType.toLowerCase();
                    if (elementType === 'multiselect') {
                        elementRef.containerViewChild.nativeElement.click();
                        elementRef.hide();
                    }
                }
                else if (isViewChild) {
                    elementRef.focus();
                }
                else {
                    /** @type {?} */
                    const element = document.querySelector(elementRef);
                    if (element) {
                        element.focus();
                    }
                }
            }), 0);
        }
    }
    /**
     * @param {?} selectedValues
     * @param {?} options
     * @return {?}
     */
    setSelectedOptionsLabel(selectedValues, options) {
        /** @type {?} */
        const values = [];
        /** @type {?} */
        let label = '';
        if (selectedValues.length) {
            selectedValues.forEach((/**
             * @param {?} value
             * @return {?}
             */
            (value) => {
                values.push(options
                    .filter((/**
                 * @param {?} item
                 * @return {?}
                 */
                (item) => item.value === value))
                    .map((/**
                 * @param {?} item
                 * @return {?}
                 */
                (item) => item.label)));
            }));
            label = values[0] + (values.length > 1 ? '...' : '');
            setTimeout((/**
             * @return {?}
             */
            () => {
                this.adjustMultiSelectDropdownLabel('with-selection-multiselect-1', values.length);
            }), 0);
        }
        return label;
    }
    /**
     * @param {?} selectedValues
     * @param {?} options
     * @return {?}
     */
    setSelectedOptionsLabelTitle(selectedValues, options) {
        /** @type {?} */
        const values = [];
        /** @type {?} */
        let title = '';
        if (selectedValues.length) {
            selectedValues.forEach((/**
             * @param {?} value
             * @return {?}
             */
            (value) => {
                values.push(options
                    .filter((/**
                 * @param {?} item
                 * @return {?}
                 */
                (item) => item.value === value))
                    .map((/**
                 * @param {?} item
                 * @return {?}
                 */
                (item) => item.label)));
            }));
            title = values.join('\n');
        }
        return title;
    }
    /**
     * @param {?=} elementId
     * @param {?=} optionsLength
     * @return {?}
     */
    adjustMultiSelectDropdownLabel(elementId = '', optionsLength = 0) {
        if (elementId) {
            setTimeout((/**
             * @return {?}
             */
            () => {
                /** @type {?} */
                const labelElementWrapper = (/** @type {?} */ (document.querySelector('#' + elementId + ' .ui-multiselect')));
                /** @type {?} */
                const labelElement = (/** @type {?} */ (document.querySelector('#' + elementId + ' .ui-multiselected-item-token span')));
                /** @type {?} */
                const minusWidth = optionsLength > 1 ? 37 : 30;
                if (labelElementWrapper && labelElement) {
                    if (labelElement.offsetWidth > (labelElementWrapper.offsetWidth - minusWidth)) {
                        labelElement.style['max-width'] = (labelElementWrapper.offsetWidth - minusWidth) + 'px';
                    }
                }
            }), 0);
        }
    }
}
CTTemplateService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
CTTemplateService.ctorParameters = () => [];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3QtdGVtcGxhdGUuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2N0LXRlbXBsYXRlLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL2N0LXRlbXBsYXRlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBSzNDLE1BQU0sT0FBTyxpQkFBaUI7SUFFN0IsZ0JBQWdCLENBQUM7Ozs7Ozs7SUFFakIsWUFBWSxDQUFDLFVBQWUsRUFBRSxXQUFXLEdBQUcsSUFBSSxFQUFFLFdBQVcsR0FBRyxFQUFFO1FBQ2pFLElBQUksVUFBVSxFQUFFO1lBQ2YsVUFBVTs7O1lBQUMsR0FBRyxFQUFFO2dCQUNmLElBQUksV0FBVyxFQUFFO29CQUNoQixXQUFXLEdBQUcsV0FBVyxDQUFDLFdBQVcsRUFBRSxDQUFDO29CQUN4QyxJQUFJLFdBQVcsS0FBSyxhQUFhLEVBQUU7d0JBQ2xDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLENBQUM7d0JBQ3BELFVBQVUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztxQkFDbEI7aUJBQ0Q7cUJBQU0sSUFBSSxXQUFXLEVBQUU7b0JBQ3ZCLFVBQVUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztpQkFDbkI7cUJBQU07OzBCQUNBLE9BQU8sR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQztvQkFDbEQsSUFBSSxPQUFPLEVBQUU7d0JBQ1osT0FBTyxDQUFDLEtBQUssRUFBRSxDQUFDO3FCQUNoQjtpQkFDRDtZQUNGLENBQUMsR0FBRSxDQUFDLENBQUMsQ0FBQztTQUNOO0lBQ0YsQ0FBQzs7Ozs7O0lBRUQsdUJBQXVCLENBQUMsY0FBd0IsRUFBRSxPQUFjOztjQUN6RCxNQUFNLEdBQUcsRUFBRTs7WUFDYixLQUFLLEdBQVcsRUFBRTtRQUV0QixJQUFJLGNBQWMsQ0FBQyxNQUFNLEVBQUU7WUFDMUIsY0FBYyxDQUFDLE9BQU87Ozs7WUFBQyxDQUFDLEtBQWEsRUFBRSxFQUFFO2dCQUN4QyxNQUFNLENBQUMsSUFBSSxDQUNWLE9BQU87cUJBQ0wsTUFBTTs7OztnQkFBQyxDQUFDLElBQWdCLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLEtBQUssS0FBSyxFQUFDO3FCQUNsRCxHQUFHOzs7O2dCQUFDLENBQUMsSUFBZ0IsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBQyxDQUN2QyxDQUFDO1lBQ0gsQ0FBQyxFQUFDLENBQUM7WUFFSCxLQUFLLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDckQsVUFBVTs7O1lBQUMsR0FBRyxFQUFFO2dCQUNmLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyw4QkFBOEIsRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDcEYsQ0FBQyxHQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ047UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNkLENBQUM7Ozs7OztJQUVELDRCQUE0QixDQUFDLGNBQXdCLEVBQUUsT0FBYzs7Y0FDOUQsTUFBTSxHQUFHLEVBQUU7O1lBQ2IsS0FBSyxHQUFXLEVBQUU7UUFFdEIsSUFBSSxjQUFjLENBQUMsTUFBTSxFQUFFO1lBQzFCLGNBQWMsQ0FBQyxPQUFPOzs7O1lBQUMsQ0FBQyxLQUFhLEVBQUUsRUFBRTtnQkFDeEMsTUFBTSxDQUFDLElBQUksQ0FDVixPQUFPO3FCQUNMLE1BQU07Ozs7Z0JBQUMsQ0FBQyxJQUFnQixFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxLQUFLLEtBQUssRUFBQztxQkFDbEQsR0FBRzs7OztnQkFBQyxDQUFDLElBQWdCLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUMsQ0FDdkMsQ0FBQztZQUNILENBQUMsRUFBQyxDQUFDO1lBRUgsS0FBSyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDMUI7UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNkLENBQUM7Ozs7OztJQUVELDhCQUE4QixDQUFDLFNBQVMsR0FBRyxFQUFFLEVBQUUsYUFBYSxHQUFHLENBQUM7UUFDL0QsSUFBSSxTQUFTLEVBQUU7WUFDZCxVQUFVOzs7WUFBQyxHQUFHLEVBQUU7O3NCQUNULG1CQUFtQixHQUFHLG1CQUFBLFFBQVEsQ0FBQyxhQUFhLENBQUMsR0FBRyxHQUFHLFNBQVMsR0FBRyxrQkFBa0IsQ0FBQyxFQUFPOztzQkFDekYsWUFBWSxHQUFHLG1CQUFBLFFBQVEsQ0FBQyxhQUFhLENBQUMsR0FBRyxHQUFHLFNBQVMsR0FBRyxvQ0FBb0MsQ0FBQyxFQUFPOztzQkFDcEcsVUFBVSxHQUFHLGFBQWEsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRTtnQkFDOUMsSUFBSSxtQkFBbUIsSUFBSSxZQUFZLEVBQUU7b0JBQ3hDLElBQUksWUFBWSxDQUFDLFdBQVcsR0FBRyxDQUFDLG1CQUFtQixDQUFDLFdBQVcsR0FBRyxVQUFVLENBQUMsRUFBRTt3QkFDOUUsWUFBWSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLFdBQVcsR0FBRyxVQUFVLENBQUMsR0FBRyxJQUFJLENBQUM7cUJBQ3hGO2lCQUNEO1lBQ0YsQ0FBQyxHQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ047SUFDRixDQUFDOzs7WUE5RUQsVUFBVSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmltcG9ydCB7IFNlbGVjdEl0ZW0gfSBmcm9tICcuLi9pbnRlcmZhY2VzL2N0LXRlbXBsYXRlLmludGVyZmFjZSc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBDVFRlbXBsYXRlU2VydmljZSB7XHJcblxyXG5cdGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG5cdHNldEF1dG9Gb2N1cyhlbGVtZW50UmVmOiBhbnksIGlzVmlld0NoaWxkID0gdHJ1ZSwgZWxlbWVudFR5cGUgPSAnJykge1xyXG5cdFx0aWYgKGVsZW1lbnRSZWYpIHtcclxuXHRcdFx0c2V0VGltZW91dCgoKSA9PiB7XHJcblx0XHRcdFx0aWYgKGVsZW1lbnRUeXBlKSB7XHJcblx0XHRcdFx0XHRlbGVtZW50VHlwZSA9IGVsZW1lbnRUeXBlLnRvTG93ZXJDYXNlKCk7XHJcblx0XHRcdFx0XHRpZiAoZWxlbWVudFR5cGUgPT09ICdtdWx0aXNlbGVjdCcpIHtcclxuXHRcdFx0XHRcdFx0ZWxlbWVudFJlZi5jb250YWluZXJWaWV3Q2hpbGQubmF0aXZlRWxlbWVudC5jbGljaygpO1xyXG5cdFx0XHRcdFx0XHRlbGVtZW50UmVmLmhpZGUoKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9IGVsc2UgaWYgKGlzVmlld0NoaWxkKSB7XHJcblx0XHRcdFx0XHRlbGVtZW50UmVmLmZvY3VzKCk7XHJcblx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdGNvbnN0IGVsZW1lbnQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGVsZW1lbnRSZWYpO1xyXG5cdFx0XHRcdFx0aWYgKGVsZW1lbnQpIHtcclxuXHRcdFx0XHRcdFx0ZWxlbWVudC5mb2N1cygpO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSwgMCk7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRzZXRTZWxlY3RlZE9wdGlvbnNMYWJlbChzZWxlY3RlZFZhbHVlczogc3RyaW5nW10sIG9wdGlvbnM6IGFueVtdKSB7XHJcblx0XHRjb25zdCB2YWx1ZXMgPSBbXTtcclxuXHRcdGxldCBsYWJlbDogc3RyaW5nID0gJyc7XHJcblxyXG5cdFx0aWYgKHNlbGVjdGVkVmFsdWVzLmxlbmd0aCkge1xyXG5cdFx0XHRzZWxlY3RlZFZhbHVlcy5mb3JFYWNoKCh2YWx1ZTogc3RyaW5nKSA9PiB7XHJcblx0XHRcdFx0dmFsdWVzLnB1c2goXHJcblx0XHRcdFx0XHRvcHRpb25zXHJcblx0XHRcdFx0XHRcdC5maWx0ZXIoKGl0ZW06IFNlbGVjdEl0ZW0pID0+IGl0ZW0udmFsdWUgPT09IHZhbHVlKVxyXG5cdFx0XHRcdFx0XHQubWFwKChpdGVtOiBTZWxlY3RJdGVtKSA9PiBpdGVtLmxhYmVsKVxyXG5cdFx0XHRcdCk7XHJcblx0XHRcdH0pO1xyXG5cclxuXHRcdFx0bGFiZWwgPSB2YWx1ZXNbMF0gKyAodmFsdWVzLmxlbmd0aCA+IDEgPyAnLi4uJyA6ICcnKTtcclxuXHRcdFx0c2V0VGltZW91dCgoKSA9PiB7XHJcblx0XHRcdFx0dGhpcy5hZGp1c3RNdWx0aVNlbGVjdERyb3Bkb3duTGFiZWwoJ3dpdGgtc2VsZWN0aW9uLW11bHRpc2VsZWN0LTEnLCB2YWx1ZXMubGVuZ3RoKTtcclxuXHRcdFx0fSwgMCk7XHJcblx0XHR9XHJcblx0XHRyZXR1cm4gbGFiZWw7XHJcblx0fVxyXG5cclxuXHRzZXRTZWxlY3RlZE9wdGlvbnNMYWJlbFRpdGxlKHNlbGVjdGVkVmFsdWVzOiBzdHJpbmdbXSwgb3B0aW9uczogYW55W10pIHtcclxuXHRcdGNvbnN0IHZhbHVlcyA9IFtdO1xyXG5cdFx0bGV0IHRpdGxlOiBzdHJpbmcgPSAnJztcclxuXHJcblx0XHRpZiAoc2VsZWN0ZWRWYWx1ZXMubGVuZ3RoKSB7XHJcblx0XHRcdHNlbGVjdGVkVmFsdWVzLmZvckVhY2goKHZhbHVlOiBzdHJpbmcpID0+IHtcclxuXHRcdFx0XHR2YWx1ZXMucHVzaChcclxuXHRcdFx0XHRcdG9wdGlvbnNcclxuXHRcdFx0XHRcdFx0LmZpbHRlcigoaXRlbTogU2VsZWN0SXRlbSkgPT4gaXRlbS52YWx1ZSA9PT0gdmFsdWUpXHJcblx0XHRcdFx0XHRcdC5tYXAoKGl0ZW06IFNlbGVjdEl0ZW0pID0+IGl0ZW0ubGFiZWwpXHJcblx0XHRcdFx0KTtcclxuXHRcdFx0fSk7XHJcblxyXG5cdFx0XHR0aXRsZSA9IHZhbHVlcy5qb2luKCdcXG4nKTtcclxuXHRcdH1cclxuXHRcdHJldHVybiB0aXRsZTtcclxuXHR9XHJcblxyXG5cdGFkanVzdE11bHRpU2VsZWN0RHJvcGRvd25MYWJlbChlbGVtZW50SWQgPSAnJywgb3B0aW9uc0xlbmd0aCA9IDApIHtcclxuXHRcdGlmIChlbGVtZW50SWQpIHtcclxuXHRcdFx0c2V0VGltZW91dCgoKSA9PiB7XHJcblx0XHRcdFx0Y29uc3QgbGFiZWxFbGVtZW50V3JhcHBlciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyMnICsgZWxlbWVudElkICsgJyAudWktbXVsdGlzZWxlY3QnKSBhcyBhbnk7XHJcblx0XHRcdFx0Y29uc3QgbGFiZWxFbGVtZW50ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignIycgKyBlbGVtZW50SWQgKyAnIC51aS1tdWx0aXNlbGVjdGVkLWl0ZW0tdG9rZW4gc3BhbicpIGFzIGFueTtcclxuXHRcdFx0XHRjb25zdCBtaW51c1dpZHRoID0gb3B0aW9uc0xlbmd0aCA+IDEgPyAzNyA6IDMwO1xyXG5cdFx0XHRcdGlmIChsYWJlbEVsZW1lbnRXcmFwcGVyICYmIGxhYmVsRWxlbWVudCkge1xyXG5cdFx0XHRcdFx0aWYgKGxhYmVsRWxlbWVudC5vZmZzZXRXaWR0aCA+IChsYWJlbEVsZW1lbnRXcmFwcGVyLm9mZnNldFdpZHRoIC0gbWludXNXaWR0aCkpIHtcclxuXHRcdFx0XHRcdFx0bGFiZWxFbGVtZW50LnN0eWxlWydtYXgtd2lkdGgnXSA9IChsYWJlbEVsZW1lbnRXcmFwcGVyLm9mZnNldFdpZHRoIC0gbWludXNXaWR0aCkgKyAncHgnO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSwgMCk7XHJcblx0XHR9XHJcblx0fVxyXG59XHJcbiJdfQ==