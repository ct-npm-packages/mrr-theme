import { CommonModule } from '@angular/common';
import { Component, Output, EventEmitter, Input, Directive, ElementRef, Renderer2, HostListener, Injectable, NgModule } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * Generated from: lib/components/dialog-box/close-icon/dialog-box.close-icon.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class DialogBoxCloseIconComponent {
    constructor() {
        this.onClose = new EventEmitter();
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    close($event) {
        this.onClose.emit($event);
    }
}
DialogBoxCloseIconComponent.decorators = [
    { type: Component, args: [{
                selector: 'ct-template-dialog-box-close-icon',
                template: "<button (click)=\"close($event)\" class=\"ui-dialog-titlebar-icon ui-dialog-titlebar-close ui-corner-all\">\r\n\t<span class=\"pi pi-times\"></span>\r\n</button>"
            }] }
];
/** @nocollapse */
DialogBoxCloseIconComponent.ctorParameters = () => [];
DialogBoxCloseIconComponent.propDecorators = {
    onClose: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * Generated from: lib/components/filter-count/filter-count.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class FilterCountComponent {
    constructor() {
        this.selectedFilterCount = 0;
        this.filterTitle = '';
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
FilterCountComponent.decorators = [
    { type: Component, args: [{
                selector: 'ct-template-filter-count',
                template: "<span *ngIf=\"selectedFilterCount && selectedFilterCount > 0\" class=\"badge badge-info badge-display\"\r\n\t[ngClass]=\"{'badge-display-twodigit':selectedFilterCount && selectedFilterCount > 9}\"\r\n\ttitle=\"{{filterTitle}}\">{{selectedFilterCount}}\r\n</span>",
                styles: [".badge-display{height:16px;width:17px;display:inline-block!important;padding-top:3px!important}.badge-display-twodigit{width:19px}"]
            }] }
];
/** @nocollapse */
FilterCountComponent.ctorParameters = () => [];
FilterCountComponent.propDecorators = {
    selectedFilterCount: [{ type: Input }],
    filterTitle: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * Generated from: lib/directives/tooltip.directive.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// Directive to show tooltip text with contents in a list with elements in vertical order.
// Currently caters to only p-multiselect element.
class TooltipDirective {
    /**
     * @param {?} el
     * @param {?} renderer
     */
    constructor(el, renderer) {
        this.el = el;
        this.renderer = renderer;
    }
    /**
     * @return {?}
     */
    onMouseOver() {
        /** @type {?} */
        const multiSelectContainer = (/** @type {?} */ (this.el.nativeElement.querySelector('.ui-multiselect-label-container')));
        /** @type {?} */
        const memberNameContainer = (/** @type {?} */ (this.el.nativeElement.querySelector('.memberName')));
        /** @type {?} */
        const providerNameContainer = (/** @type {?} */ (this.el.nativeElement.querySelector('.providerName')));
        if (multiSelectContainer) {
            /** @type {?} */
            let title = multiSelectContainer.getAttribute('title');
            /** @type {?} */
            const hasFilterList = this.el.nativeElement.classList.contains('filteredTooltipList');
            if (hasFilterList && this.ngModel) {
                title = '';
                this.ngModel.forEach((/**
                 * @param {?} item
                 * @return {?}
                 */
                (item) => {
                    title += item.value + '\n';
                }));
            }
            else {
                title = title.split(', ').join('\n');
            }
            this.renderer.setProperty(multiSelectContainer, 'title', title);
        }
        else if (memberNameContainer) {
            /** @type {?} */
            const title = memberNameContainer.getAttribute('title');
            /** @type {?} */
            const titleArr = title.split('- ');
            if (titleArr.length > 1) {
                this.renderer.setProperty(memberNameContainer, 'title', titleArr.join('\n'));
            }
        }
        else if (providerNameContainer) {
            /** @type {?} */
            const title = providerNameContainer.getAttribute('title');
            /** @type {?} */
            const titleArr = title.split('- ');
            if (titleArr.length > 1) {
                /** @type {?} */
                const providerNpi = titleArr.pop();
                this.renderer.setProperty(providerNameContainer, 'title', titleArr.pop() + '\n' + providerNpi);
            }
        }
        else {
            /** @type {?} */
            const title = this.el.nativeElement.getAttribute('title');
            /** @type {?} */
            const titleArr = title.split(', ');
            if (titleArr.length > 1) {
                /** @type {?} */
                const titleString = titleArr.join('\n');
                this.renderer.setProperty(this.el.nativeElement, 'title', titleString);
            }
        }
    }
}
TooltipDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ctTemplateTooltip]'
            },] }
];
/** @nocollapse */
TooltipDirective.ctorParameters = () => [
    { type: ElementRef },
    { type: Renderer2 }
];
TooltipDirective.propDecorators = {
    ngModel: [{ type: Input }],
    onMouseOver: [{ type: HostListener, args: ['mouseover',] }]
};

/**
 * @fileoverview added by tsickle
 * Generated from: lib/services/ct-template.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CTTemplateService {
    constructor() { }
    /**
     * @param {?} elementRef
     * @param {?=} isViewChild
     * @param {?=} elementType
     * @return {?}
     */
    setAutoFocus(elementRef, isViewChild = true, elementType = '') {
        if (elementRef) {
            setTimeout((/**
             * @return {?}
             */
            () => {
                if (elementType) {
                    elementType = elementType.toLowerCase();
                    if (elementType === 'multiselect') {
                        elementRef.containerViewChild.nativeElement.click();
                        elementRef.hide();
                    }
                }
                else if (isViewChild) {
                    elementRef.focus();
                }
                else {
                    /** @type {?} */
                    const element = document.querySelector(elementRef);
                    if (element) {
                        element.focus();
                    }
                }
            }), 0);
        }
    }
    /**
     * @param {?} selectedValues
     * @param {?} options
     * @return {?}
     */
    setSelectedOptionsLabel(selectedValues, options) {
        /** @type {?} */
        const values = [];
        /** @type {?} */
        let label = '';
        if (selectedValues.length) {
            selectedValues.forEach((/**
             * @param {?} value
             * @return {?}
             */
            (value) => {
                values.push(options
                    .filter((/**
                 * @param {?} item
                 * @return {?}
                 */
                (item) => item.value === value))
                    .map((/**
                 * @param {?} item
                 * @return {?}
                 */
                (item) => item.label)));
            }));
            label = values[0] + (values.length > 1 ? '...' : '');
            setTimeout((/**
             * @return {?}
             */
            () => {
                this.adjustMultiSelectDropdownLabel('with-selection-multiselect-1', values.length);
            }), 0);
        }
        return label;
    }
    /**
     * @param {?} selectedValues
     * @param {?} options
     * @return {?}
     */
    setSelectedOptionsLabelTitle(selectedValues, options) {
        /** @type {?} */
        const values = [];
        /** @type {?} */
        let title = '';
        if (selectedValues.length) {
            selectedValues.forEach((/**
             * @param {?} value
             * @return {?}
             */
            (value) => {
                values.push(options
                    .filter((/**
                 * @param {?} item
                 * @return {?}
                 */
                (item) => item.value === value))
                    .map((/**
                 * @param {?} item
                 * @return {?}
                 */
                (item) => item.label)));
            }));
            title = values.join('\n');
        }
        return title;
    }
    /**
     * @param {?=} elementId
     * @param {?=} optionsLength
     * @return {?}
     */
    adjustMultiSelectDropdownLabel(elementId = '', optionsLength = 0) {
        if (elementId) {
            setTimeout((/**
             * @return {?}
             */
            () => {
                /** @type {?} */
                const labelElementWrapper = (/** @type {?} */ (document.querySelector('#' + elementId + ' .ui-multiselect')));
                /** @type {?} */
                const labelElement = (/** @type {?} */ (document.querySelector('#' + elementId + ' .ui-multiselected-item-token span')));
                /** @type {?} */
                const minusWidth = optionsLength > 1 ? 37 : 30;
                if (labelElementWrapper && labelElement) {
                    if (labelElement.offsetWidth > (labelElementWrapper.offsetWidth - minusWidth)) {
                        labelElement.style['max-width'] = (labelElementWrapper.offsetWidth - minusWidth) + 'px';
                    }
                }
            }), 0);
        }
    }
}
CTTemplateService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
CTTemplateService.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/ct-template.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CTTemplateModule {
}
CTTemplateModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    DialogBoxCloseIconComponent,
                    FilterCountComponent,
                    TooltipDirective
                ],
                imports: [
                    CommonModule
                ],
                exports: [
                    DialogBoxCloseIconComponent,
                    FilterCountComponent,
                    TooltipDirective
                ],
                providers: [
                    CTTemplateService
                ]
            },] }
];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/interfaces/ct-template.interface.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: public-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: ct-template.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { CTTemplateModule, CTTemplateService, DialogBoxCloseIconComponent as ɵa, FilterCountComponent as ɵb, TooltipDirective as ɵc };

//# sourceMappingURL=ct-template.js.map