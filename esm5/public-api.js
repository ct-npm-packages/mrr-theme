/**
 * @fileoverview added by tsickle
 * Generated from: public-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
 * Public API Surface of ct-template
 */
export { CTTemplateModule } from './lib/ct-template.module';
export { CTTemplateService } from './lib/services/ct-template.service';
export {} from './lib/interfaces/ct-template.interface';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljLWFwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2N0LXRlbXBsYXRlLyIsInNvdXJjZXMiOlsicHVibGljLWFwaS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUlBLGlDQUFjLDBCQUEwQixDQUFDO0FBQ3pDLGtDQUFjLG9DQUFvQyxDQUFDO0FBQ25ELGVBQWMsd0NBQXdDLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG4gKiBQdWJsaWMgQVBJIFN1cmZhY2Ugb2YgY3QtdGVtcGxhdGVcclxuICovXHJcblxyXG5leHBvcnQgKiBmcm9tICcuL2xpYi9jdC10ZW1wbGF0ZS5tb2R1bGUnO1xyXG5leHBvcnQgKiBmcm9tICcuL2xpYi9zZXJ2aWNlcy9jdC10ZW1wbGF0ZS5zZXJ2aWNlJztcclxuZXhwb3J0ICogZnJvbSAnLi9saWIvaW50ZXJmYWNlcy9jdC10ZW1wbGF0ZS5pbnRlcmZhY2UnO1xyXG4iXX0=