/**
 * @fileoverview added by tsickle
 * Generated from: lib/services/ct-template.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
var CTTemplateService = /** @class */ (function () {
    function CTTemplateService() {
    }
    /**
     * @param {?} elementRef
     * @param {?=} isViewChild
     * @param {?=} elementType
     * @return {?}
     */
    CTTemplateService.prototype.setAutoFocus = /**
     * @param {?} elementRef
     * @param {?=} isViewChild
     * @param {?=} elementType
     * @return {?}
     */
    function (elementRef, isViewChild, elementType) {
        if (isViewChild === void 0) { isViewChild = true; }
        if (elementType === void 0) { elementType = ''; }
        if (elementRef) {
            setTimeout((/**
             * @return {?}
             */
            function () {
                if (elementType) {
                    elementType = elementType.toLowerCase();
                    if (elementType === 'multiselect') {
                        elementRef.containerViewChild.nativeElement.click();
                        elementRef.hide();
                    }
                }
                else if (isViewChild) {
                    elementRef.focus();
                }
                else {
                    /** @type {?} */
                    var element = document.querySelector(elementRef);
                    if (element) {
                        element.focus();
                    }
                }
            }), 0);
        }
    };
    /**
     * @param {?} selectedValues
     * @param {?} options
     * @return {?}
     */
    CTTemplateService.prototype.setSelectedOptionsLabel = /**
     * @param {?} selectedValues
     * @param {?} options
     * @return {?}
     */
    function (selectedValues, options) {
        var _this = this;
        /** @type {?} */
        var values = [];
        /** @type {?} */
        var label = '';
        if (selectedValues.length) {
            selectedValues.forEach((/**
             * @param {?} value
             * @return {?}
             */
            function (value) {
                values.push(options
                    .filter((/**
                 * @param {?} item
                 * @return {?}
                 */
                function (item) { return item.value === value; }))
                    .map((/**
                 * @param {?} item
                 * @return {?}
                 */
                function (item) { return item.label; })));
            }));
            label = values[0] + (values.length > 1 ? '...' : '');
            setTimeout((/**
             * @return {?}
             */
            function () {
                _this.adjustMultiSelectDropdownLabel('with-selection-multiselect-1', values.length);
            }), 0);
        }
        return label;
    };
    /**
     * @param {?} selectedValues
     * @param {?} options
     * @return {?}
     */
    CTTemplateService.prototype.setSelectedOptionsLabelTitle = /**
     * @param {?} selectedValues
     * @param {?} options
     * @return {?}
     */
    function (selectedValues, options) {
        /** @type {?} */
        var values = [];
        /** @type {?} */
        var title = '';
        if (selectedValues.length) {
            selectedValues.forEach((/**
             * @param {?} value
             * @return {?}
             */
            function (value) {
                values.push(options
                    .filter((/**
                 * @param {?} item
                 * @return {?}
                 */
                function (item) { return item.value === value; }))
                    .map((/**
                 * @param {?} item
                 * @return {?}
                 */
                function (item) { return item.label; })));
            }));
            title = values.join('\n');
        }
        return title;
    };
    /**
     * @param {?=} elementId
     * @param {?=} optionsLength
     * @return {?}
     */
    CTTemplateService.prototype.adjustMultiSelectDropdownLabel = /**
     * @param {?=} elementId
     * @param {?=} optionsLength
     * @return {?}
     */
    function (elementId, optionsLength) {
        if (elementId === void 0) { elementId = ''; }
        if (optionsLength === void 0) { optionsLength = 0; }
        if (elementId) {
            setTimeout((/**
             * @return {?}
             */
            function () {
                /** @type {?} */
                var labelElementWrapper = (/** @type {?} */ (document.querySelector('#' + elementId + ' .ui-multiselect')));
                /** @type {?} */
                var labelElement = (/** @type {?} */ (document.querySelector('#' + elementId + ' .ui-multiselected-item-token span')));
                /** @type {?} */
                var minusWidth = optionsLength > 1 ? 37 : 30;
                if (labelElementWrapper && labelElement) {
                    if (labelElement.offsetWidth > (labelElementWrapper.offsetWidth - minusWidth)) {
                        labelElement.style['max-width'] = (labelElementWrapper.offsetWidth - minusWidth) + 'px';
                    }
                }
            }), 0);
        }
    };
    CTTemplateService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    CTTemplateService.ctorParameters = function () { return []; };
    return CTTemplateService;
}());
export { CTTemplateService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3QtdGVtcGxhdGUuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2N0LXRlbXBsYXRlLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL2N0LXRlbXBsYXRlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBSTNDO0lBR0M7SUFBZ0IsQ0FBQzs7Ozs7OztJQUVqQix3Q0FBWTs7Ozs7O0lBQVosVUFBYSxVQUFlLEVBQUUsV0FBa0IsRUFBRSxXQUFnQjtRQUFwQyw0QkFBQSxFQUFBLGtCQUFrQjtRQUFFLDRCQUFBLEVBQUEsZ0JBQWdCO1FBQ2pFLElBQUksVUFBVSxFQUFFO1lBQ2YsVUFBVTs7O1lBQUM7Z0JBQ1YsSUFBSSxXQUFXLEVBQUU7b0JBQ2hCLFdBQVcsR0FBRyxXQUFXLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQ3hDLElBQUksV0FBVyxLQUFLLGFBQWEsRUFBRTt3QkFDbEMsVUFBVSxDQUFDLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQzt3QkFDcEQsVUFBVSxDQUFDLElBQUksRUFBRSxDQUFDO3FCQUNsQjtpQkFDRDtxQkFBTSxJQUFJLFdBQVcsRUFBRTtvQkFDdkIsVUFBVSxDQUFDLEtBQUssRUFBRSxDQUFDO2lCQUNuQjtxQkFBTTs7d0JBQ0EsT0FBTyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDO29CQUNsRCxJQUFJLE9BQU8sRUFBRTt3QkFDWixPQUFPLENBQUMsS0FBSyxFQUFFLENBQUM7cUJBQ2hCO2lCQUNEO1lBQ0YsQ0FBQyxHQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ047SUFDRixDQUFDOzs7Ozs7SUFFRCxtREFBdUI7Ozs7O0lBQXZCLFVBQXdCLGNBQXdCLEVBQUUsT0FBYztRQUFoRSxpQkFtQkM7O1lBbEJNLE1BQU0sR0FBRyxFQUFFOztZQUNiLEtBQUssR0FBVyxFQUFFO1FBRXRCLElBQUksY0FBYyxDQUFDLE1BQU0sRUFBRTtZQUMxQixjQUFjLENBQUMsT0FBTzs7OztZQUFDLFVBQUMsS0FBYTtnQkFDcEMsTUFBTSxDQUFDLElBQUksQ0FDVixPQUFPO3FCQUNMLE1BQU07Ozs7Z0JBQUMsVUFBQyxJQUFnQixJQUFLLE9BQUEsSUFBSSxDQUFDLEtBQUssS0FBSyxLQUFLLEVBQXBCLENBQW9CLEVBQUM7cUJBQ2xELEdBQUc7Ozs7Z0JBQUMsVUFBQyxJQUFnQixJQUFLLE9BQUEsSUFBSSxDQUFDLEtBQUssRUFBVixDQUFVLEVBQUMsQ0FDdkMsQ0FBQztZQUNILENBQUMsRUFBQyxDQUFDO1lBRUgsS0FBSyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ3JELFVBQVU7OztZQUFDO2dCQUNWLEtBQUksQ0FBQyw4QkFBOEIsQ0FBQyw4QkFBOEIsRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDcEYsQ0FBQyxHQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ047UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNkLENBQUM7Ozs7OztJQUVELHdEQUE0Qjs7Ozs7SUFBNUIsVUFBNkIsY0FBd0IsRUFBRSxPQUFjOztZQUM5RCxNQUFNLEdBQUcsRUFBRTs7WUFDYixLQUFLLEdBQVcsRUFBRTtRQUV0QixJQUFJLGNBQWMsQ0FBQyxNQUFNLEVBQUU7WUFDMUIsY0FBYyxDQUFDLE9BQU87Ozs7WUFBQyxVQUFDLEtBQWE7Z0JBQ3BDLE1BQU0sQ0FBQyxJQUFJLENBQ1YsT0FBTztxQkFDTCxNQUFNOzs7O2dCQUFDLFVBQUMsSUFBZ0IsSUFBSyxPQUFBLElBQUksQ0FBQyxLQUFLLEtBQUssS0FBSyxFQUFwQixDQUFvQixFQUFDO3FCQUNsRCxHQUFHOzs7O2dCQUFDLFVBQUMsSUFBZ0IsSUFBSyxPQUFBLElBQUksQ0FBQyxLQUFLLEVBQVYsQ0FBVSxFQUFDLENBQ3ZDLENBQUM7WUFDSCxDQUFDLEVBQUMsQ0FBQztZQUVILEtBQUssR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQzFCO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDZCxDQUFDOzs7Ozs7SUFFRCwwREFBOEI7Ozs7O0lBQTlCLFVBQStCLFNBQWMsRUFBRSxhQUFpQjtRQUFqQywwQkFBQSxFQUFBLGNBQWM7UUFBRSw4QkFBQSxFQUFBLGlCQUFpQjtRQUMvRCxJQUFJLFNBQVMsRUFBRTtZQUNkLFVBQVU7OztZQUFDOztvQkFDSixtQkFBbUIsR0FBRyxtQkFBQSxRQUFRLENBQUMsYUFBYSxDQUFDLEdBQUcsR0FBRyxTQUFTLEdBQUcsa0JBQWtCLENBQUMsRUFBTzs7b0JBQ3pGLFlBQVksR0FBRyxtQkFBQSxRQUFRLENBQUMsYUFBYSxDQUFDLEdBQUcsR0FBRyxTQUFTLEdBQUcsb0NBQW9DLENBQUMsRUFBTzs7b0JBQ3BHLFVBQVUsR0FBRyxhQUFhLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0JBQzlDLElBQUksbUJBQW1CLElBQUksWUFBWSxFQUFFO29CQUN4QyxJQUFJLFlBQVksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLEdBQUcsVUFBVSxDQUFDLEVBQUU7d0JBQzlFLFlBQVksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLEdBQUcsVUFBVSxDQUFDLEdBQUcsSUFBSSxDQUFDO3FCQUN4RjtpQkFDRDtZQUNGLENBQUMsR0FBRSxDQUFDLENBQUMsQ0FBQztTQUNOO0lBQ0YsQ0FBQzs7Z0JBOUVELFVBQVU7Ozs7SUErRVgsd0JBQUM7Q0FBQSxBQS9FRCxJQStFQztTQTlFWSxpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBTZWxlY3RJdGVtIH0gZnJvbSAnLi4vaW50ZXJmYWNlcy9jdC10ZW1wbGF0ZS5pbnRlcmZhY2UnO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgQ1RUZW1wbGF0ZVNlcnZpY2Uge1xyXG5cclxuXHRjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuXHRzZXRBdXRvRm9jdXMoZWxlbWVudFJlZjogYW55LCBpc1ZpZXdDaGlsZCA9IHRydWUsIGVsZW1lbnRUeXBlID0gJycpIHtcclxuXHRcdGlmIChlbGVtZW50UmVmKSB7XHJcblx0XHRcdHNldFRpbWVvdXQoKCkgPT4ge1xyXG5cdFx0XHRcdGlmIChlbGVtZW50VHlwZSkge1xyXG5cdFx0XHRcdFx0ZWxlbWVudFR5cGUgPSBlbGVtZW50VHlwZS50b0xvd2VyQ2FzZSgpO1xyXG5cdFx0XHRcdFx0aWYgKGVsZW1lbnRUeXBlID09PSAnbXVsdGlzZWxlY3QnKSB7XHJcblx0XHRcdFx0XHRcdGVsZW1lbnRSZWYuY29udGFpbmVyVmlld0NoaWxkLm5hdGl2ZUVsZW1lbnQuY2xpY2soKTtcclxuXHRcdFx0XHRcdFx0ZWxlbWVudFJlZi5oaWRlKCk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSBlbHNlIGlmIChpc1ZpZXdDaGlsZCkge1xyXG5cdFx0XHRcdFx0ZWxlbWVudFJlZi5mb2N1cygpO1xyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRjb25zdCBlbGVtZW50ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihlbGVtZW50UmVmKTtcclxuXHRcdFx0XHRcdGlmIChlbGVtZW50KSB7XHJcblx0XHRcdFx0XHRcdGVsZW1lbnQuZm9jdXMoKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdH0sIDApO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0c2V0U2VsZWN0ZWRPcHRpb25zTGFiZWwoc2VsZWN0ZWRWYWx1ZXM6IHN0cmluZ1tdLCBvcHRpb25zOiBhbnlbXSkge1xyXG5cdFx0Y29uc3QgdmFsdWVzID0gW107XHJcblx0XHRsZXQgbGFiZWw6IHN0cmluZyA9ICcnO1xyXG5cclxuXHRcdGlmIChzZWxlY3RlZFZhbHVlcy5sZW5ndGgpIHtcclxuXHRcdFx0c2VsZWN0ZWRWYWx1ZXMuZm9yRWFjaCgodmFsdWU6IHN0cmluZykgPT4ge1xyXG5cdFx0XHRcdHZhbHVlcy5wdXNoKFxyXG5cdFx0XHRcdFx0b3B0aW9uc1xyXG5cdFx0XHRcdFx0XHQuZmlsdGVyKChpdGVtOiBTZWxlY3RJdGVtKSA9PiBpdGVtLnZhbHVlID09PSB2YWx1ZSlcclxuXHRcdFx0XHRcdFx0Lm1hcCgoaXRlbTogU2VsZWN0SXRlbSkgPT4gaXRlbS5sYWJlbClcclxuXHRcdFx0XHQpO1xyXG5cdFx0XHR9KTtcclxuXHJcblx0XHRcdGxhYmVsID0gdmFsdWVzWzBdICsgKHZhbHVlcy5sZW5ndGggPiAxID8gJy4uLicgOiAnJyk7XHJcblx0XHRcdHNldFRpbWVvdXQoKCkgPT4ge1xyXG5cdFx0XHRcdHRoaXMuYWRqdXN0TXVsdGlTZWxlY3REcm9wZG93bkxhYmVsKCd3aXRoLXNlbGVjdGlvbi1tdWx0aXNlbGVjdC0xJywgdmFsdWVzLmxlbmd0aCk7XHJcblx0XHRcdH0sIDApO1xyXG5cdFx0fVxyXG5cdFx0cmV0dXJuIGxhYmVsO1xyXG5cdH1cclxuXHJcblx0c2V0U2VsZWN0ZWRPcHRpb25zTGFiZWxUaXRsZShzZWxlY3RlZFZhbHVlczogc3RyaW5nW10sIG9wdGlvbnM6IGFueVtdKSB7XHJcblx0XHRjb25zdCB2YWx1ZXMgPSBbXTtcclxuXHRcdGxldCB0aXRsZTogc3RyaW5nID0gJyc7XHJcblxyXG5cdFx0aWYgKHNlbGVjdGVkVmFsdWVzLmxlbmd0aCkge1xyXG5cdFx0XHRzZWxlY3RlZFZhbHVlcy5mb3JFYWNoKCh2YWx1ZTogc3RyaW5nKSA9PiB7XHJcblx0XHRcdFx0dmFsdWVzLnB1c2goXHJcblx0XHRcdFx0XHRvcHRpb25zXHJcblx0XHRcdFx0XHRcdC5maWx0ZXIoKGl0ZW06IFNlbGVjdEl0ZW0pID0+IGl0ZW0udmFsdWUgPT09IHZhbHVlKVxyXG5cdFx0XHRcdFx0XHQubWFwKChpdGVtOiBTZWxlY3RJdGVtKSA9PiBpdGVtLmxhYmVsKVxyXG5cdFx0XHRcdCk7XHJcblx0XHRcdH0pO1xyXG5cclxuXHRcdFx0dGl0bGUgPSB2YWx1ZXMuam9pbignXFxuJyk7XHJcblx0XHR9XHJcblx0XHRyZXR1cm4gdGl0bGU7XHJcblx0fVxyXG5cclxuXHRhZGp1c3RNdWx0aVNlbGVjdERyb3Bkb3duTGFiZWwoZWxlbWVudElkID0gJycsIG9wdGlvbnNMZW5ndGggPSAwKSB7XHJcblx0XHRpZiAoZWxlbWVudElkKSB7XHJcblx0XHRcdHNldFRpbWVvdXQoKCkgPT4ge1xyXG5cdFx0XHRcdGNvbnN0IGxhYmVsRWxlbWVudFdyYXBwZXIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjJyArIGVsZW1lbnRJZCArICcgLnVpLW11bHRpc2VsZWN0JykgYXMgYW55O1xyXG5cdFx0XHRcdGNvbnN0IGxhYmVsRWxlbWVudCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyMnICsgZWxlbWVudElkICsgJyAudWktbXVsdGlzZWxlY3RlZC1pdGVtLXRva2VuIHNwYW4nKSBhcyBhbnk7XHJcblx0XHRcdFx0Y29uc3QgbWludXNXaWR0aCA9IG9wdGlvbnNMZW5ndGggPiAxID8gMzcgOiAzMDtcclxuXHRcdFx0XHRpZiAobGFiZWxFbGVtZW50V3JhcHBlciAmJiBsYWJlbEVsZW1lbnQpIHtcclxuXHRcdFx0XHRcdGlmIChsYWJlbEVsZW1lbnQub2Zmc2V0V2lkdGggPiAobGFiZWxFbGVtZW50V3JhcHBlci5vZmZzZXRXaWR0aCAtIG1pbnVzV2lkdGgpKSB7XHJcblx0XHRcdFx0XHRcdGxhYmVsRWxlbWVudC5zdHlsZVsnbWF4LXdpZHRoJ10gPSAobGFiZWxFbGVtZW50V3JhcHBlci5vZmZzZXRXaWR0aCAtIG1pbnVzV2lkdGgpICsgJ3B4JztcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdH0sIDApO1xyXG5cdFx0fVxyXG5cdH1cclxufVxyXG4iXX0=