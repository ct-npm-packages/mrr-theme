/**
 * @fileoverview added by tsickle
 * Generated from: lib/ct-template.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// Components
import { DialogBoxCloseIconComponent } from './components/dialog-box/close-icon/dialog-box.close-icon.component';
import { FilterCountComponent } from './components/filter-count/filter-count.component';
// Directives
import { TooltipDirective } from './directives/tooltip.directive';
// Services
import { CTTemplateService } from './services/ct-template.service';
var CTTemplateModule = /** @class */ (function () {
    function CTTemplateModule() {
    }
    CTTemplateModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [
                        DialogBoxCloseIconComponent,
                        FilterCountComponent,
                        TooltipDirective
                    ],
                    imports: [
                        CommonModule
                    ],
                    exports: [
                        DialogBoxCloseIconComponent,
                        FilterCountComponent,
                        TooltipDirective
                    ],
                    providers: [
                        CTTemplateService
                    ]
                },] }
    ];
    return CTTemplateModule;
}());
export { CTTemplateModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3QtdGVtcGxhdGUubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vY3QtdGVtcGxhdGUvIiwic291cmNlcyI6WyJsaWIvY3QtdGVtcGxhdGUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUNBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDOztBQUcvQyxPQUFPLEVBQUUsMkJBQTJCLEVBQUUsTUFBTSxvRUFBb0UsQ0FBQztBQUNqSCxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxrREFBa0QsQ0FBQzs7QUFHeEYsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sZ0NBQWdDLENBQUM7O0FBR2xFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBRW5FO0lBQUE7SUFrQmdDLENBQUM7O2dCQWxCaEMsUUFBUSxTQUFDO29CQUNULFlBQVksRUFBRTt3QkFDYiwyQkFBMkI7d0JBQzNCLG9CQUFvQjt3QkFDcEIsZ0JBQWdCO3FCQUNoQjtvQkFDRCxPQUFPLEVBQUU7d0JBQ1IsWUFBWTtxQkFDWjtvQkFDRCxPQUFPLEVBQUU7d0JBQ1IsMkJBQTJCO3dCQUMzQixvQkFBb0I7d0JBQ3BCLGdCQUFnQjtxQkFDaEI7b0JBQ0QsU0FBUyxFQUFFO3dCQUNWLGlCQUFpQjtxQkFDakI7aUJBQ0Q7O0lBQytCLHVCQUFDO0NBQUEsQUFsQmpDLElBa0JpQztTQUFwQixnQkFBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBBbmd1bGFyXHJcbmltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcblxyXG4vLyBDb21wb25lbnRzXHJcbmltcG9ydCB7IERpYWxvZ0JveENsb3NlSWNvbkNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9kaWFsb2ctYm94L2Nsb3NlLWljb24vZGlhbG9nLWJveC5jbG9zZS1pY29uLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEZpbHRlckNvdW50Q29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2ZpbHRlci1jb3VudC9maWx0ZXItY291bnQuY29tcG9uZW50JztcclxuXHJcbi8vIERpcmVjdGl2ZXNcclxuaW1wb3J0IHsgVG9vbHRpcERpcmVjdGl2ZSB9IGZyb20gJy4vZGlyZWN0aXZlcy90b29sdGlwLmRpcmVjdGl2ZSc7XHJcblxyXG4vLyBTZXJ2aWNlc1xyXG5pbXBvcnQgeyBDVFRlbXBsYXRlU2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvY3QtdGVtcGxhdGUuc2VydmljZSc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG5cdGRlY2xhcmF0aW9uczogW1xyXG5cdFx0RGlhbG9nQm94Q2xvc2VJY29uQ29tcG9uZW50LFxyXG5cdFx0RmlsdGVyQ291bnRDb21wb25lbnQsXHJcblx0XHRUb29sdGlwRGlyZWN0aXZlXHJcblx0XSxcclxuXHRpbXBvcnRzOiBbXHJcblx0XHRDb21tb25Nb2R1bGVcclxuXHRdLFxyXG5cdGV4cG9ydHM6IFtcclxuXHRcdERpYWxvZ0JveENsb3NlSWNvbkNvbXBvbmVudCxcclxuXHRcdEZpbHRlckNvdW50Q29tcG9uZW50LFxyXG5cdFx0VG9vbHRpcERpcmVjdGl2ZVxyXG5cdF0sXHJcblx0cHJvdmlkZXJzOiBbXHJcblx0XHRDVFRlbXBsYXRlU2VydmljZVxyXG5cdF1cclxufSlcclxuZXhwb3J0IGNsYXNzIENUVGVtcGxhdGVNb2R1bGUgeyB9XHJcbiJdfQ==