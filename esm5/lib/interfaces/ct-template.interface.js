/**
 * @fileoverview added by tsickle
 * Generated from: lib/interfaces/ct-template.interface.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function MultiSelectToggleItem() { }
if (false) {
    /** @type {?} */
    MultiSelectToggleItem.prototype.itemValue;
    /** @type {?} */
    MultiSelectToggleItem.prototype.originalEvent;
    /** @type {?} */
    MultiSelectToggleItem.prototype.value;
}
/**
 * @record
 */
export function SelectItem() { }
if (false) {
    /** @type {?|undefined} */
    SelectItem.prototype.label;
    /** @type {?} */
    SelectItem.prototype.value;
    /** @type {?|undefined} */
    SelectItem.prototype.styleClass;
    /** @type {?|undefined} */
    SelectItem.prototype.icon;
    /** @type {?|undefined} */
    SelectItem.prototype.title;
    /** @type {?|undefined} */
    SelectItem.prototype.disabled;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3QtdGVtcGxhdGUuaW50ZXJmYWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vY3QtdGVtcGxhdGUvIiwic291cmNlcyI6WyJsaWIvaW50ZXJmYWNlcy9jdC10ZW1wbGF0ZS5pbnRlcmZhY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQSwyQ0FJQzs7O0lBSEEsMENBQWtCOztJQUNsQiw4Q0FBMEI7O0lBQzFCLHNDQUFnQjs7Ozs7QUFHakIsZ0NBT0M7OztJQU5BLDJCQUFlOztJQUNmLDJCQUFXOztJQUNYLGdDQUFvQjs7SUFDcEIsMEJBQWM7O0lBQ2QsMkJBQWU7O0lBQ2YsOEJBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGludGVyZmFjZSBNdWx0aVNlbGVjdFRvZ2dsZUl0ZW0ge1xyXG5cdGl0ZW1WYWx1ZTogc3RyaW5nO1xyXG5cdG9yaWdpbmFsRXZlbnQ6IE1vdXNlRXZlbnQ7XHJcblx0dmFsdWU6IHN0cmluZ1tdO1xyXG59XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIFNlbGVjdEl0ZW0ge1xyXG5cdGxhYmVsPzogc3RyaW5nO1xyXG5cdHZhbHVlOiBhbnk7XHJcblx0c3R5bGVDbGFzcz86IHN0cmluZztcclxuXHRpY29uPzogc3RyaW5nO1xyXG5cdHRpdGxlPzogc3RyaW5nO1xyXG5cdGRpc2FibGVkPzogYm9vbGVhbjtcclxufVxyXG4iXX0=