/**
 * @fileoverview added by tsickle
 * Generated from: lib/directives/tooltip.directive.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, ElementRef, Renderer2, HostListener, Input } from '@angular/core';
// Directive to show tooltip text with contents in a list with elements in vertical order.
// Currently caters to only p-multiselect element.
var TooltipDirective = /** @class */ (function () {
    function TooltipDirective(el, renderer) {
        this.el = el;
        this.renderer = renderer;
    }
    /**
     * @return {?}
     */
    TooltipDirective.prototype.onMouseOver = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var multiSelectContainer = (/** @type {?} */ (this.el.nativeElement.querySelector('.ui-multiselect-label-container')));
        /** @type {?} */
        var memberNameContainer = (/** @type {?} */ (this.el.nativeElement.querySelector('.memberName')));
        /** @type {?} */
        var providerNameContainer = (/** @type {?} */ (this.el.nativeElement.querySelector('.providerName')));
        if (multiSelectContainer) {
            /** @type {?} */
            var title_1 = multiSelectContainer.getAttribute('title');
            /** @type {?} */
            var hasFilterList = this.el.nativeElement.classList.contains('filteredTooltipList');
            if (hasFilterList && this.ngModel) {
                title_1 = '';
                this.ngModel.forEach((/**
                 * @param {?} item
                 * @return {?}
                 */
                function (item) {
                    title_1 += item.value + '\n';
                }));
            }
            else {
                title_1 = title_1.split(', ').join('\n');
            }
            this.renderer.setProperty(multiSelectContainer, 'title', title_1);
        }
        else if (memberNameContainer) {
            /** @type {?} */
            var title = memberNameContainer.getAttribute('title');
            /** @type {?} */
            var titleArr = title.split('- ');
            if (titleArr.length > 1) {
                this.renderer.setProperty(memberNameContainer, 'title', titleArr.join('\n'));
            }
        }
        else if (providerNameContainer) {
            /** @type {?} */
            var title = providerNameContainer.getAttribute('title');
            /** @type {?} */
            var titleArr = title.split('- ');
            if (titleArr.length > 1) {
                /** @type {?} */
                var providerNpi = titleArr.pop();
                this.renderer.setProperty(providerNameContainer, 'title', titleArr.pop() + '\n' + providerNpi);
            }
        }
        else {
            /** @type {?} */
            var title = this.el.nativeElement.getAttribute('title');
            /** @type {?} */
            var titleArr = title.split(', ');
            if (titleArr.length > 1) {
                /** @type {?} */
                var titleString = titleArr.join('\n');
                this.renderer.setProperty(this.el.nativeElement, 'title', titleString);
            }
        }
    };
    TooltipDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[ctTemplateTooltip]'
                },] }
    ];
    /** @nocollapse */
    TooltipDirective.ctorParameters = function () { return [
        { type: ElementRef },
        { type: Renderer2 }
    ]; };
    TooltipDirective.propDecorators = {
        ngModel: [{ type: Input }],
        onMouseOver: [{ type: HostListener, args: ['mouseover',] }]
    };
    return TooltipDirective;
}());
export { TooltipDirective };
if (false) {
    /** @type {?} */
    TooltipDirective.prototype.ngModel;
    /**
     * @type {?}
     * @private
     */
    TooltipDirective.prototype.el;
    /**
     * @type {?}
     * @private
     */
    TooltipDirective.prototype.renderer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9vbHRpcC5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9jdC10ZW1wbGF0ZS8iLCJzb3VyY2VzIjpbImxpYi9kaXJlY3RpdmVzL3Rvb2x0aXAuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7OztBQUl0RjtJQU1DLDBCQUFvQixFQUFjLEVBQ3pCLFFBQW1CO1FBRFIsT0FBRSxHQUFGLEVBQUUsQ0FBWTtRQUN6QixhQUFRLEdBQVIsUUFBUSxDQUFXO0lBQzVCLENBQUM7Ozs7SUFFMEIsc0NBQVc7OztJQUF0Qzs7WUFDTyxvQkFBb0IsR0FBZ0IsbUJBQUEsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLGlDQUFpQyxDQUFDLEVBQWU7O1lBQ3pILG1CQUFtQixHQUFnQixtQkFBQSxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLEVBQWU7O1lBQ3BHLHFCQUFxQixHQUFnQixtQkFBQSxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsZUFBZSxDQUFDLEVBQWU7UUFFOUcsSUFBSSxvQkFBb0IsRUFBRTs7Z0JBQ3JCLE9BQUssR0FBRyxvQkFBb0IsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDOztnQkFDaEQsYUFBYSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMscUJBQXFCLENBQUM7WUFDckYsSUFBSSxhQUFhLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtnQkFDbEMsT0FBSyxHQUFHLEVBQUUsQ0FBQztnQkFDWCxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU87Ozs7Z0JBQUMsVUFBQyxJQUFTO29CQUM5QixPQUFLLElBQUksSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7Z0JBQzVCLENBQUMsRUFBQyxDQUFDO2FBQ0g7aUJBQU07Z0JBQ04sT0FBSyxHQUFHLE9BQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ3JDO1lBQ0QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsb0JBQW9CLEVBQUUsT0FBTyxFQUFFLE9BQUssQ0FBQyxDQUFDO1NBQ2hFO2FBQU0sSUFBSSxtQkFBbUIsRUFBRTs7Z0JBQ3pCLEtBQUssR0FBRyxtQkFBbUIsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDOztnQkFDakQsUUFBUSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDO1lBRWxDLElBQUksUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ3hCLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLG1CQUFtQixFQUFFLE9BQU8sRUFBRSxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7YUFDN0U7U0FDRDthQUFNLElBQUkscUJBQXFCLEVBQUU7O2dCQUMzQixLQUFLLEdBQUcscUJBQXFCLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQzs7Z0JBQ25ELFFBQVEsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztZQUVsQyxJQUFJLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFOztvQkFDbEIsV0FBVyxHQUFHLFFBQVEsQ0FBQyxHQUFHLEVBQUU7Z0JBQ2xDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLHFCQUFxQixFQUFFLE9BQU8sRUFBRSxRQUFRLENBQUMsR0FBRyxFQUFFLEdBQUcsSUFBSSxHQUFHLFdBQVcsQ0FBQyxDQUFDO2FBQy9GO1NBQ0Q7YUFBTTs7Z0JBQ0EsS0FBSyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUM7O2dCQUNuRCxRQUFRLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUM7WUFFbEMsSUFBSSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTs7b0JBQ2xCLFdBQVcsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztnQkFDdkMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLEVBQUUsT0FBTyxFQUFFLFdBQVcsQ0FBQyxDQUFDO2FBQ3ZFO1NBQ0Q7SUFDRixDQUFDOztnQkFuREQsU0FBUyxTQUFDO29CQUNWLFFBQVEsRUFBRSxxQkFBcUI7aUJBQy9COzs7O2dCQU5tQixVQUFVO2dCQUFFLFNBQVM7OzswQkFRdkMsS0FBSzs4QkFNTCxZQUFZLFNBQUMsV0FBVzs7SUEwQzFCLHVCQUFDO0NBQUEsQUFwREQsSUFvREM7U0FqRFksZ0JBQWdCOzs7SUFDNUIsbUNBQXNCOzs7OztJQUVWLDhCQUFzQjs7Ozs7SUFDakMsb0NBQTJCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlLCBFbGVtZW50UmVmLCBSZW5kZXJlcjIsIEhvc3RMaXN0ZW5lciwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbi8vIERpcmVjdGl2ZSB0byBzaG93IHRvb2x0aXAgdGV4dCB3aXRoIGNvbnRlbnRzIGluIGEgbGlzdCB3aXRoIGVsZW1lbnRzIGluIHZlcnRpY2FsIG9yZGVyLlxyXG4vLyBDdXJyZW50bHkgY2F0ZXJzIHRvIG9ubHkgcC1tdWx0aXNlbGVjdCBlbGVtZW50LlxyXG5ARGlyZWN0aXZlKHtcclxuXHRzZWxlY3RvcjogJ1tjdFRlbXBsYXRlVG9vbHRpcF0nXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBUb29sdGlwRGlyZWN0aXZlIHtcclxuXHRASW5wdXQoKSBuZ01vZGVsOiBhbnk7XHJcblxyXG5cdGNvbnN0cnVjdG9yKHByaXZhdGUgZWw6IEVsZW1lbnRSZWYsXHJcblx0XHRwcml2YXRlIHJlbmRlcmVyOiBSZW5kZXJlcjIpIHtcclxuXHR9XHJcblxyXG5cdEBIb3N0TGlzdGVuZXIoJ21vdXNlb3ZlcicpIG9uTW91c2VPdmVyKCkge1xyXG5cdFx0Y29uc3QgbXVsdGlTZWxlY3RDb250YWluZXI6IEhUTUxFbGVtZW50ID0gdGhpcy5lbC5uYXRpdmVFbGVtZW50LnF1ZXJ5U2VsZWN0b3IoJy51aS1tdWx0aXNlbGVjdC1sYWJlbC1jb250YWluZXInKSBhcyBIVE1MRWxlbWVudDtcclxuXHRcdGNvbnN0IG1lbWJlck5hbWVDb250YWluZXI6IEhUTUxFbGVtZW50ID0gdGhpcy5lbC5uYXRpdmVFbGVtZW50LnF1ZXJ5U2VsZWN0b3IoJy5tZW1iZXJOYW1lJykgYXMgSFRNTEVsZW1lbnQ7XHJcblx0XHRjb25zdCBwcm92aWRlck5hbWVDb250YWluZXI6IEhUTUxFbGVtZW50ID0gdGhpcy5lbC5uYXRpdmVFbGVtZW50LnF1ZXJ5U2VsZWN0b3IoJy5wcm92aWRlck5hbWUnKSBhcyBIVE1MRWxlbWVudDtcclxuXHJcblx0XHRpZiAobXVsdGlTZWxlY3RDb250YWluZXIpIHtcclxuXHRcdFx0bGV0IHRpdGxlID0gbXVsdGlTZWxlY3RDb250YWluZXIuZ2V0QXR0cmlidXRlKCd0aXRsZScpO1xyXG5cdFx0XHRjb25zdCBoYXNGaWx0ZXJMaXN0ID0gdGhpcy5lbC5uYXRpdmVFbGVtZW50LmNsYXNzTGlzdC5jb250YWlucygnZmlsdGVyZWRUb29sdGlwTGlzdCcpO1xyXG5cdFx0XHRpZiAoaGFzRmlsdGVyTGlzdCAmJiB0aGlzLm5nTW9kZWwpIHtcclxuXHRcdFx0XHR0aXRsZSA9ICcnO1xyXG5cdFx0XHRcdHRoaXMubmdNb2RlbC5mb3JFYWNoKChpdGVtOiBhbnkpID0+IHtcclxuXHRcdFx0XHRcdHRpdGxlICs9IGl0ZW0udmFsdWUgKyAnXFxuJztcclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHR0aXRsZSA9IHRpdGxlLnNwbGl0KCcsICcpLmpvaW4oJ1xcbicpO1xyXG5cdFx0XHR9XHJcblx0XHRcdHRoaXMucmVuZGVyZXIuc2V0UHJvcGVydHkobXVsdGlTZWxlY3RDb250YWluZXIsICd0aXRsZScsIHRpdGxlKTtcclxuXHRcdH0gZWxzZSBpZiAobWVtYmVyTmFtZUNvbnRhaW5lcikge1xyXG5cdFx0XHRjb25zdCB0aXRsZSA9IG1lbWJlck5hbWVDb250YWluZXIuZ2V0QXR0cmlidXRlKCd0aXRsZScpO1xyXG5cdFx0XHRjb25zdCB0aXRsZUFyciA9IHRpdGxlLnNwbGl0KCctICcpO1xyXG5cclxuXHRcdFx0aWYgKHRpdGxlQXJyLmxlbmd0aCA+IDEpIHtcclxuXHRcdFx0XHR0aGlzLnJlbmRlcmVyLnNldFByb3BlcnR5KG1lbWJlck5hbWVDb250YWluZXIsICd0aXRsZScsIHRpdGxlQXJyLmpvaW4oJ1xcbicpKTtcclxuXHRcdFx0fVxyXG5cdFx0fSBlbHNlIGlmIChwcm92aWRlck5hbWVDb250YWluZXIpIHtcclxuXHRcdFx0Y29uc3QgdGl0bGUgPSBwcm92aWRlck5hbWVDb250YWluZXIuZ2V0QXR0cmlidXRlKCd0aXRsZScpO1xyXG5cdFx0XHRjb25zdCB0aXRsZUFyciA9IHRpdGxlLnNwbGl0KCctICcpO1xyXG5cclxuXHRcdFx0aWYgKHRpdGxlQXJyLmxlbmd0aCA+IDEpIHtcclxuXHRcdFx0XHRjb25zdCBwcm92aWRlck5waSA9IHRpdGxlQXJyLnBvcCgpO1xyXG5cdFx0XHRcdHRoaXMucmVuZGVyZXIuc2V0UHJvcGVydHkocHJvdmlkZXJOYW1lQ29udGFpbmVyLCAndGl0bGUnLCB0aXRsZUFyci5wb3AoKSArICdcXG4nICsgcHJvdmlkZXJOcGkpO1xyXG5cdFx0XHR9XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHRjb25zdCB0aXRsZSA9IHRoaXMuZWwubmF0aXZlRWxlbWVudC5nZXRBdHRyaWJ1dGUoJ3RpdGxlJyk7XHJcblx0XHRcdGNvbnN0IHRpdGxlQXJyID0gdGl0bGUuc3BsaXQoJywgJyk7XHJcblxyXG5cdFx0XHRpZiAodGl0bGVBcnIubGVuZ3RoID4gMSkge1xyXG5cdFx0XHRcdGNvbnN0IHRpdGxlU3RyaW5nID0gdGl0bGVBcnIuam9pbignXFxuJyk7XHJcblx0XHRcdFx0dGhpcy5yZW5kZXJlci5zZXRQcm9wZXJ0eSh0aGlzLmVsLm5hdGl2ZUVsZW1lbnQsICd0aXRsZScsIHRpdGxlU3RyaW5nKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxufVxyXG4iXX0=