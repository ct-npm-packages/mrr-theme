/**
 * @fileoverview added by tsickle
 * Generated from: lib/components/dialog-box/close-icon/dialog-box.close-icon.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Output, EventEmitter } from '@angular/core';
var DialogBoxCloseIconComponent = /** @class */ (function () {
    function DialogBoxCloseIconComponent() {
        this.onClose = new EventEmitter();
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    DialogBoxCloseIconComponent.prototype.close = /**
     * @param {?} $event
     * @return {?}
     */
    function ($event) {
        this.onClose.emit($event);
    };
    DialogBoxCloseIconComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ct-template-dialog-box-close-icon',
                    template: "<button (click)=\"close($event)\" class=\"ui-dialog-titlebar-icon ui-dialog-titlebar-close ui-corner-all\">\r\n\t<span class=\"pi pi-times\"></span>\r\n</button>"
                }] }
    ];
    /** @nocollapse */
    DialogBoxCloseIconComponent.ctorParameters = function () { return []; };
    DialogBoxCloseIconComponent.propDecorators = {
        onClose: [{ type: Output }]
    };
    return DialogBoxCloseIconComponent;
}());
export { DialogBoxCloseIconComponent };
if (false) {
    /** @type {?} */
    DialogBoxCloseIconComponent.prototype.onClose;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlhbG9nLWJveC5jbG9zZS1pY29uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2N0LXRlbXBsYXRlLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvZGlhbG9nLWJveC9jbG9zZS1pY29uL2RpYWxvZy1ib3guY2xvc2UtaWNvbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFaEU7SUFRQztRQUZVLFlBQU8sR0FBNkIsSUFBSSxZQUFZLEVBQUUsQ0FBQztJQUVqRCxDQUFDOzs7OztJQUVqQiwyQ0FBSzs7OztJQUFMLFVBQU0sTUFBa0I7UUFDdkIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDM0IsQ0FBQzs7Z0JBWkQsU0FBUyxTQUFDO29CQUNWLFFBQVEsRUFBRSxtQ0FBbUM7b0JBQzdDLDZLQUFxRDtpQkFDckQ7Ozs7OzBCQUdDLE1BQU07O0lBT1Isa0NBQUM7Q0FBQSxBQWJELElBYUM7U0FUWSwyQkFBMkI7OztJQUV2Qyw4Q0FBaUUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE91dHB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuXHRzZWxlY3RvcjogJ2N0LXRlbXBsYXRlLWRpYWxvZy1ib3gtY2xvc2UtaWNvbicsXHJcblx0dGVtcGxhdGVVcmw6ICcuL2RpYWxvZy1ib3guY2xvc2UtaWNvbi5jb21wb25lbnQuaHRtbCcsXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBEaWFsb2dCb3hDbG9zZUljb25Db21wb25lbnQge1xyXG5cclxuXHRAT3V0cHV0KCkgb25DbG9zZTogRXZlbnRFbWl0dGVyPE1vdXNlRXZlbnQ+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG5cclxuXHRjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuXHRjbG9zZSgkZXZlbnQ6IE1vdXNlRXZlbnQpIHtcclxuXHRcdHRoaXMub25DbG9zZS5lbWl0KCRldmVudCk7XHJcblx0fVxyXG59XHJcbiJdfQ==