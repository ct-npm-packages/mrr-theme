/**
 * @fileoverview added by tsickle
 * Generated from: lib/components/filter-count/filter-count.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
var FilterCountComponent = /** @class */ (function () {
    function FilterCountComponent() {
        this.selectedFilterCount = 0;
        this.filterTitle = '';
    }
    /**
     * @return {?}
     */
    FilterCountComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    FilterCountComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ct-template-filter-count',
                    template: "<span *ngIf=\"selectedFilterCount && selectedFilterCount > 0\" class=\"badge badge-info badge-display\"\r\n\t[ngClass]=\"{'badge-display-twodigit':selectedFilterCount && selectedFilterCount > 9}\"\r\n\ttitle=\"{{filterTitle}}\">{{selectedFilterCount}}\r\n</span>",
                    styles: [".badge-display{height:16px;width:17px;display:inline-block!important;padding-top:3px!important}.badge-display-twodigit{width:19px}"]
                }] }
    ];
    /** @nocollapse */
    FilterCountComponent.ctorParameters = function () { return []; };
    FilterCountComponent.propDecorators = {
        selectedFilterCount: [{ type: Input }],
        filterTitle: [{ type: Input }]
    };
    return FilterCountComponent;
}());
export { FilterCountComponent };
if (false) {
    /** @type {?} */
    FilterCountComponent.prototype.selectedFilterCount;
    /** @type {?} */
    FilterCountComponent.prototype.filterTitle;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsdGVyLWNvdW50LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2N0LXRlbXBsYXRlLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvZmlsdGVyLWNvdW50L2ZpbHRlci1jb3VudC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUV6RDtJQVVDO1FBSFMsd0JBQW1CLEdBQVcsQ0FBQyxDQUFDO1FBQ2hDLGdCQUFXLEdBQVcsRUFBRSxDQUFDO0lBRWxCLENBQUM7Ozs7SUFFakIsdUNBQVE7OztJQUFSO0lBQ0EsQ0FBQzs7Z0JBYkQsU0FBUyxTQUFDO29CQUNWLFFBQVEsRUFBRSwwQkFBMEI7b0JBQ3BDLGtSQUE0Qzs7aUJBRTVDOzs7OztzQ0FHQyxLQUFLOzhCQUNMLEtBQUs7O0lBT1AsMkJBQUM7Q0FBQSxBQWZELElBZUM7U0FWWSxvQkFBb0I7OztJQUVoQyxtREFBeUM7O0lBQ3pDLDJDQUFrQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcblx0c2VsZWN0b3I6ICdjdC10ZW1wbGF0ZS1maWx0ZXItY291bnQnLFxyXG5cdHRlbXBsYXRlVXJsOiAnLi9maWx0ZXItY291bnQuY29tcG9uZW50Lmh0bWwnLFxyXG5cdHN0eWxlVXJsczogWycuL2ZpbHRlci1jb3VudC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGaWx0ZXJDb3VudENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG5cdEBJbnB1dCgpIHNlbGVjdGVkRmlsdGVyQ291bnQ6IE51bWJlciA9IDA7XHJcblx0QElucHV0KCkgZmlsdGVyVGl0bGU6IFN0cmluZyA9ICcnO1xyXG5cclxuXHRjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuXHRuZ09uSW5pdCgpIHtcclxuXHR9XHJcblxyXG59XHJcbiJdfQ==